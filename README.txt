Welcome to Persistence4J
==========================
Persistence4j is a very simple library which can be used to persist data into Relational databases.This library frees you from writing the Data transfer objects. Just create a POJO and simply save it using this library.I have also integrate Java transaction API with this library
The library is written 100% in the Java language and minimum Java 1.6 is required. 

LICENSE
========
This software up to version 1.2 was released under LGPL. Release version 1.5 onwards the license changed to Apache 2.0.The Apache 2.0 license is sufficiently flexible to allow the use of this software in both open source and commercial projects. 

Supported Databases
===================
Apache Derby
HSQLDB
MySQL
PostgreSQL 

Sample Application
===================
A Sample Application is there to demonstrate how to use the Persistence4J framework.This sample web application is build using GWT library and can be deployed to any servlet 2.5 compatible container e.g tomcat, Jetty etc.

Show me examples?
================
Note:- Check Sample Application for more complex uses cases and features of Persistence4J

A typical use of Persistence4J.

//First lets create a simple pojo which you like to persist.
@Entity(schema="library",table="book")
public class Book{
@Column(isPrimaryKey=true)
private String isbn;
@Column
private String title;
@Column
private int authorid;
public Book(){

}
public Book(String isbn, String title, int authorid){
  this.isbn = isbn;
  this.title = title;
  this.authorid = authorid;
}
// getters
}

DataProviderFactory dataProviderFactory = new DataProviderFactoryImpl(config);
String databaseName = "library";
String dbmsName = "mysql"
boolean isTransactional = false;
DataProvider dataProvider =  dataProviderFactory.getDataProvider(databaseName, dbmsName, isTransactional);

// Now lets create a object of Book class and persist it
Book book = new Book("123432","TestBook",5);
TransferUtil.registerClass(Book.class);
GenericDAO<Book> genericDAO = new GenericDaoImpl<Book>(dataProvider.getDataFetcher());

//Persist Book
genericDAO.createEntity(book);

//Remove Book
genericDAO.deleteEntity(book);

//Test if Entity Exists
genericDAO.isEntityExists(book);

// findByPrimaryKey
Object obj[] = new Object[1];
obj[0] = "123432";
genericDAO.findByPrimaryKey(Book.class, obj);

//If you want to use transactions.This how to get TransactionService.Make sure //isTransactional variable should be true and underlying dbms supports ACID.
TransactionService ts = dataProvider.getTransactionService();
try{
    ts.beginTransaction();
    genericDAO.createEntity(book);
    ts.commitTransaction();
}catch(Exception exp){
ts.rollbackTransaction();
}

// Check the GenericDAO interface for all the available methods..
You can extend GenericDAOImpl and override the methods and add your own methods.

Dependencies
============
Check Persistence4J Dependencies.Add these libraries to class path.If you are using Maven to build your project then maven automatically downloads the Dependencies.

Questions and Comments
======================
Please post at http://groups.google.com/group/persistence4j.

Maven
=====
To use Persistence4J via a maven repository, configure your pom.xml with

 <repository>
    <id>persistence4j-repo</id>
    <name>Persistence4J Maven2 Repository</name>
    <url>http://persistence4j.googlecode.com/svn/repo</url>
</repository>

Add dependencies to Persistence4J in your pom.xml
 <dependency>
      <groupId>com.avdy</groupId>
      <artifactId>persistence4j-jdbc</artifactId>
      <version>1.5</version>
 </dependency>
 <dependency>
     <groupId>com.avdy</groupId>
     <artifactId>persistence4j-common</artifactId>
     <version>1.5</version>
 </dependency>
 
 Building
=========
Checkout the source code and run following command from the command line.
$ mvn clean install

Thanks
Avdhesh Yadav