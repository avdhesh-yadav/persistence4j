/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.transfer;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.avdy.p4j.jdbc.model.Column;
import com.avdy.p4j.jdbc.model.Component;
import com.avdy.p4j.jdbc.service.GenericDTO;


/**
 * 
 * @author Avdhesh yadav
 */
class Transformer
{
	/** */
	private String fullTableName;
	
	/**
	 * 
	 * @param fullTableName String
	 */
	public Transformer(String fullTableName)
	{
		this.fullTableName = fullTableName;
	}
	
	
	/**
	 * 
	 * @param genericDTO GenericDTO
	 * 
	 * @return Object
	 * 
	 * @throws Exception
	 */
	public Object toEntityObject(GenericDTO genericDTO , Object entityObject) throws Exception
	{
		Field fields [] = entityObject.getClass().getDeclaredFields();
		List<Field> entityCompFields = getComponentFields(fields);
		
		Map<String , Object> data = genericDTO.getData();
		Set<String> columns = data.keySet();
		for(String columnName : columns)
		{
			Field field = isFieldColumn(columnName, fields);
			if(field != null)
			{
				setFieldData(field, entityObject, data, columnName);
			}
			else if(entityCompFields.size() > 0)
			{
				boolean columnFound = false;
				for(Field entityCompField :  entityCompFields)
				{
					entityCompField.setAccessible(true);
					Object componentObj =  entityCompField.get(entityObject);
					Field compFields [] = componentObj.getClass().getDeclaredFields();
					for(Field compField : compFields)
					{
						if(compField.isAnnotationPresent(Column.class) && columnName.equalsIgnoreCase(compField.getName()))
						{
							setFieldData(compField, componentObj, data, columnName);
							columnFound = true;
							break;
						}
					}
					if(columnFound)
					{
						break;
					}
				}
			}
		}
		return entityObject;
	}
	
	
	/**
	 * 
	 * @param entityObject Object
	 * 
	 * @return GenericDTO
	 * 
	 * @throws Exception
	 */
	public GenericDTO toGenericDTO(Object entityObject) throws Exception
	{
		Field fields [] = entityObject.getClass().getDeclaredFields();
		
		Map<String, Object> data =  new HashMap<String, Object>();
		for(Field field : fields)
		{
			if(field.isAnnotationPresent(Column.class))
			{
				Object objs[] = getFieldData(field,entityObject);
				data.put(String.valueOf(objs[0]), objs[1]);
			}
			else if(field.isAnnotationPresent(Component.class))
			{
				field.setAccessible(true);
				Object componentObj =  field.get(entityObject);
				Field compFields [] = componentObj.getClass().getDeclaredFields();
				for(Field compField : compFields)
				{
					if(compField.isAnnotationPresent(Column.class))
					{
						Object objs[] = getFieldData(compField,componentObj);
						data.put(String.valueOf(objs[0]), objs[1]);
					}
				}
			}
		}
		GenericDTO genericDTO = new GenericDTO(fullTableName, data);
		return genericDTO;
	}	
	
	
	/**
	 * 
	 * @param fields Field[]
	 * 
	 * @return List<Field>
	 */
	private List<Field> getComponentFields(Field[] fields)
	{
		List<Field> componentFields = new ArrayList<Field>();
		for(Field field : fields)
		{
			if(field.isAnnotationPresent(Component.class))
			{
				componentFields.add(field);
			}
		}
		return componentFields;
	}
	
	
	/**
	 * 
	 * @param column String
	 * @param fields Field[]
	 * 
	 * @return Field
	 */
	private Field isFieldColumn(String column,Field[] fields)
	{
		for(Field field : fields)
		{
			if(field.isAnnotationPresent(Column.class) && column.equalsIgnoreCase(field.getName()))
			{
				return field;
			}
		}
		return null;
	}
	
	
	/**
	 * 
	 * @param field Field
	 * @param entityObject Object
	 * @param data Map<String , Object> data
	 * @param columnName String
	 * 
	 * @throws IllegalAccessException
	 */
	private void setFieldData(Field field , Object entityObject,Map<String , Object> data , String columnName) throws Exception
	{
		if(field != null)
		{
			columnName = columnName.toLowerCase();
			field.setAccessible(true);
			Object obj = data.get(columnName);
			field.set(entityObject, obj);
		}
		else
		{
			throw new Exception(columnName + " This columName is not defiend in EntityObject:voClass:"+entityObject.getClass());
		}
	}
	
	
	/**
	 * 
	 * @param field Field
	 * @param object Object
	 * 
	 * @return Object[]
	 * 
	 * @throws Exception
	 */
	private  Object[] getFieldData(Field field , Object object) throws Exception
	{
		Object data[] = new Object[2];
		String columnName = field.getName();
		columnName = columnName.toLowerCase();
		field.setAccessible(true);
		Object obj =  field.get(object);
		Column column = (Column)field.getAnnotation(Column.class);
		if((!column.isNullable() && obj == null) || (column.isPrimaryKey() && obj == null))
			throw new Exception("value for " + columnName + " can not be null for table: EntityClass " + object.getClass());
		data[0] =  columnName;
		data[1] = obj;
		return data;
	}
}