/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms.connfactory;

import com.avdy.p4j.common.DbmsType;
import com.avdy.p4j.jdbc.dbms.DataSourceAttr;

/**
 * @author Avdhesh Yadav - Avdhesh.Yadav@Gmail.com    
 */
public abstract class ConnectionFactory 
{
	
	/**
	 * @param xaApiProvider String
	 * @param database String
	 * @param dsAttr DataSourceAttr
	 * 
	 * @return XaDBConnector
	 * 
	 * @throws Exception
	 */
	public abstract XaDBConnector newXADBConnector(String xaApiProvider, String database, DataSourceAttr dsAttr) throws Exception;
	
	/**
	 * 
	 * @param database String
	 * @param dsAttr DataSourceAttr
	 * 
	 * @return DBConnector
	 * @throws Exception
	 */
	public abstract DBConnector newNonXADBConnector(String database, DataSourceAttr dsAttr) throws Exception;
	
	/**
	 * 
	 * @param whichFactory String
	 * 
	 * @return ConnectionFactory ConnectionFactory
	 */
	public static ConnectionFactory getConnectionFactory(String whichFactory)
	{
		ConnectionFactory factory = null;
		
		if(whichFactory.equalsIgnoreCase(DbmsType.MYSQL))
		{
			factory = DefaultConnectionFactory.getInstance();
		}
		else if(whichFactory.equalsIgnoreCase(DbmsType.PGSQL))
		{
			factory = DefaultConnectionFactory.getInstance();
		}
		else if(whichFactory.equalsIgnoreCase(DbmsType.DERBY))
		{
			factory = DerbyConnectionFactory.getInstance();
		}
		else if(whichFactory.equalsIgnoreCase(DbmsType.HSQLDB))
		{
			factory = DerbyConnectionFactory.getInstance();
		}
		return factory;
	}
}