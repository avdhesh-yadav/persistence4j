/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dao;

import java.util.List;
import java.util.Map;

import com.avdy.p4j.common.DAOException;
import com.avdy.p4j.jdbc.model.DTO;
import com.avdy.p4j.jdbc.service.DataFetcher;

/**
 * 
 * @author Avdhesh Yadav
 *
 */
public class DataFetcherImpl implements DataFetcher
{
	private static final long serialVersionUID = 1L;
	/** */
	private AbsPersistenceManager persistenceManager;
	
	/**
	 * 
	 * @param pcManager AbsPersistenceManager

	 * @throws Exception
	 */
	public DataFetcherImpl(AbsPersistenceManager pcManager)throws Exception
	{
		this.persistenceManager = pcManager;
	}
		
	
	/**
	 * 
	 * @param DTO dto
	 * 
	 * @return boolean
	 * 
	 * @throws DAOException
	 */
	public boolean isEntityExists(DTO dto) throws DAOException
	{
		return persistenceManager.isEntityExists(dto);
	}
	
	
	/**
	 * @param DTO dto
	 */
	public void save(DTO dto)throws DAOException
	{
		persistenceManager.persistEntity(dto);
	}
	
	
	/**
	 * This method first try to persist this entity.if this entity already exist 
	 * in the database then the database entity will be updated.
	 * 
	 * Note:- Method only works for the entities which have the primary key.
	 * 
	 * @param dto DTO 
	 * 
	 * @throws RemoteException
	 * @throws DAOException
	 */
	public void saveOrUpdate(DTO dto) throws DAOException
	{
		boolean isEntityExists = persistenceManager.isEntityExists(dto);
		if(isEntityExists)
			persistenceManager.deleteEntity(dto);
		
		// Create New Entity
		persistenceManager.persistEntity(dto);
	}
	
	
	/**
	 * @param className String
	 */
	public List<DTO> load(String tableName) throws DAOException
	{
		return persistenceManager.loadALLEntities(tableName);
	}
	
	
	/**
	 * 
	 */
	public List<? extends DTO> load(String tableName , int startPosition , int rows) throws DAOException
	{
		List<DTO> l = (List<DTO>)persistenceManager.loadEntities(tableName, null, null, startPosition, rows);
		return l;
	}
	
	
	/**
	 * 
	 */
	public DTO findByPrimaryKey(String tableName,Object obj[]) throws DAOException
	{
		return persistenceManager.findByPrimaryKey(tableName, obj);
	}
	
	
	/**
	 * 
	 */
	public List<DTO> findByAttribute(String tableName,String param, String value, String orderByField, String ascDesc,int startPosition, int rows) throws DAOException
	{
		return persistenceManager.findByAttribute(tableName,param, value, orderByField, ascDesc, startPosition, rows);
	}
	
	
	/**
	 * 
	 */
	public List<DTO> findByQuery(String tableName, String query, int startPosition,int rows) throws DAOException
	{
		return persistenceManager.findByQuery(tableName, query, startPosition, rows);
	}

	
	/**
	 * 
	 */
	public DTO updateEntity(DTO vo, Map<String,Object> whereClause) throws DAOException
	{
		return persistenceManager.updateEntity(vo, whereClause);
	}
	
	
	/**
	 * 
	 */
	public void deleteEntity(DTO dto) throws DAOException
	{
		persistenceManager.deleteEntity(dto);
	}
	
	
	/**
	 * 
	 */
	public List<DTO> find(String tableName,String[] params,String values[],String orderByField,String ascDesc,int startPosition,int rows ) throws Exception
	{
		return persistenceManager.find(tableName, params, values, orderByField, ascDesc, startPosition, rows);
	}
	
	
	/**
	 * 
	 */
	public int countRows(String tableName,String params,String value) throws DAOException
	{
		return persistenceManager.countRows(tableName, params, value);
	}
	
	
	/**
	 * 
	 * @param dtos List<DTO>
	 * 
	 * @return Map<Integer, DTO>
	 * 
	 * @throws DAOException
	 * @throws RemoteException
	 */
	public Map<Integer, DTO> batchInsert(List<DTO> dtos) throws DAOException
	{
		return persistenceManager.batchInsert(dtos);
	}
	
	
	/**
	 * 
	 */
	public List<? extends DTO> executeQueries(String tableName, List<String> queries) throws DAOException
	{
		return persistenceManager.executeQueries(tableName, queries);		
	}
	
	
	/**
	 * 
	 * @param query String
	 * 
	 * @return boolean
	 * 
	 * @throws RemoteException
	 * @throws DAOException
	 */
	public boolean executeQuery(String query) throws DAOException
	{
		return persistenceManager.executeQuery(query);
	}
}
