/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc;

import java.util.Properties;


/**
 * This is immutable object so it can be shared without any problem in the system.
 * 
 * @author Avdhesh yadav
 */
public class PersistenceConfig
{
	/** */
	private Properties mProps;
	
	private static final String mContainerName = "persistence";
	
	
	/**
	 * 
	 * @param fileReader PropertyFileReader
	 */
	public PersistenceConfig(Properties props)
	{
		mProps = props;
	}
	

	/**
	 * 
	 * @return String
	 */
	public String getXaApiProvider()
	{
		return mProps.getProperty(mContainerName+".xaprovider");
	}
	
	/**
	 * 
	 * @return String
	 */
	public String getDbmss()
	{
		return mProps.getProperty(mContainerName + ".dbmss");
	}
	
	
	/**
	 * @param dbms  String
	 * 
	 * @return String
	 */
	public String getDatabaseDriver(String dbms)
	{
		return mProps.getProperty(mContainerName+".dbms."+ dbms+ ".driver");
	}
	
	
	/**
	 * 
	 * @param dbms  String
	 * 
	 * @return  String
	 */
	public String getHostName(String dbms)
	{
		return mProps.getProperty(mContainerName+".dbms." + dbms + ".hostname","localhost");
	}
	
	/**
	 * 
	 * @param dbms  String
	 * 
	 * @return  int
	 */
	public int getDataBasePort(String dbms)
	{
		return Integer.parseInt(mProps.getProperty(mContainerName+".dbms." + dbms + ".port"));
	}

	
	/**
	 * 
	 * @param dbms String
	 * 
	 * @return String
	 */
	public String getUserName(String dbms)
	{
		return mProps.getProperty(mContainerName+".dbms." + dbms + ".username","root");
	}
	
	
	/**
	 * 
	 * @param dbms String
	 * 
	 * @return String
	 */
	public String getPassword(String dbms)
	{
		return mProps.getProperty(mContainerName+".dbms." + dbms + ".password","");
	}
	
	
	/**
	 * @param dbms String
	 * 
	 * @return int
	 */
	public int getMinPool(String dbms)
	{
		return  Integer.parseInt(mProps.getProperty(mContainerName+".dbms." + dbms + ".minpool","10"));
	}
	
	
	/**
	 * @param dbms String
	 * 
	 * @return int
	 */
	public int getMaxPool(String dbms)
	{
		return  Integer.parseInt(mProps.getProperty(mContainerName+".dbms." + dbms + ".maxpool","30"));
	}
	
	
	/**
	 * @param dbms String
	 * @return String
	 */
	public String getXaDataSourceClassName(String dbms)
	{
		return mProps.getProperty(mContainerName+".dbms." + dbms + ".XaDataSourceClassName");
	}
}