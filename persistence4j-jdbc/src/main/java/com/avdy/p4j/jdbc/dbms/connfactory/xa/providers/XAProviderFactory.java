/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms.connfactory.xa.providers;

import com.avdy.p4j.jdbc.dbms.DataSourceAttr;
import com.avdy.p4j.jdbc.dbms.connfactory.xa.XAProvider;
import com.avdy.p4j.jdbc.dbms.connfactory.xa.providers.AtomikosXAProviderimpl;

/**
 * 
 * @author Avdhesh Yadav
 */
public abstract class XAProviderFactory 
{
	//
	public static final String ATOMIKOS = "atomikos";
	
	/**
	 * 
	 * @param apiProvider String
	 * @param database String
	 * @param dsAttr DataSourceAttr
	 * 
	 * @return XAProvider
	 * 
	 * @throws Exception
	 */
	public static XAProvider getXAProvider(String apiProvider,String database, DataSourceAttr dsAttr)throws Exception
	{
		XAProvider xaProvider = null;
		if(apiProvider.equalsIgnoreCase(ATOMIKOS))
		{
			xaProvider = new AtomikosXAProviderimpl(database, dsAttr);
		}
		else
		{
			throw new Exception("Sorry this "+ apiProvider  + " apiprovider is not supported yet:");
		}
		return xaProvider;
	}
}