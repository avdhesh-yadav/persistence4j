/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms.connfactory.xa;

import java.sql.Connection;
import java.sql.SQLException;

import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import com.avdy.p4j.jdbc.dbms.DataSourceAttr;
import com.avdy.p4j.jdbc.dbms.connfactory.xa.XAProvider;
import com.avdy.p4j.jdbc.dbms.connfactory.xa.providers.XAProviderFactory;

/**
 * 
 * @author Avdhesh Yadav
 *
 */
public class AbsXaDBConnector 
{
	private static Logger logger = Logger.getLogger(AbsXaDBConnector.class.getName());
	//
	protected String m_Database;
	//
	protected XAProvider mXaProvider;

	/**
	 * @param provider String
	 * @param database String
	 * @param dsAttr DataSourceAttr
	 * 
	 * 
	 * @throws Exception
	 */
	public AbsXaDBConnector(String provider,String database, DataSourceAttr dsAttr)throws Exception
	{
		m_Database = database;
		mXaProvider  = XAProviderFactory.getXAProvider(provider,database,dsAttr);
	}
	
	/**
	 * 
	 * @return Connection
	 * 
	 * @throws Exception
	 * 
	 */
	public Connection getConnection() 
	{
		logger.debug("getting connection from XA pool");
		try 
		{
			return mXaProvider.getConnection();
		} 
		catch (SQLException e) 
		{
			logger.error("getting connection from XA pool:"+e.getMessage());
		}
		return null;
	}

	
	/**
	 * 
	 * @return UserTransaction
	 */
	public UserTransaction getUserTransaction()
	{
		return mXaProvider.getXAUserTransaction();
	}
	
	
	/**
	 * @return String
	 */
	public String getDatabaseName()
	{
		return m_Database;
	}
	
	
	/**
	 * This method stops the connection pool and  TMService
	 */
	public void stop()
	{
		mXaProvider.stop();
	}
}
