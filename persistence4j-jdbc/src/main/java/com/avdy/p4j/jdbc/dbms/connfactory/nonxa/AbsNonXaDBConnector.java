/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms.connfactory.nonxa;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import com.avdy.p4j.jdbc.dbms.DataSourceAttr;

/**
 * 
 * @author Avdhesh Yadav
 *
 */
abstract class AbsNonXaDBConnector
{
	public static final String DEFAULT_VALIDATION_QUERY = "select 1";
	
	private String validationQuery;
	protected String m_Database;
	protected DataSourceAttr mDsAttr;
	private DataSource mDataSource;

	
	/**
	 * 
	 * @param database String
	 * @param validationQuery String
	 * @param dsAttr DataSourceAttr
	 */
	public AbsNonXaDBConnector(String database, String validationQuery, DataSourceAttr dsAttr)
	{
		m_Database = database;
		this.validationQuery = validationQuery;
		mDsAttr = dsAttr;
		initilizeDatasource();
	}

	
	/**
	 * 
	 */
	private void initilizeDatasource()
	{
		mDataSource = new BasicDataSource();
		((BasicDataSource)mDataSource).setDriverClassName(mDsAttr.getDriverClassName());
		((BasicDataSource)mDataSource).setUsername(mDsAttr.getUserName());
		((BasicDataSource)mDataSource).setPassword(mDsAttr.getPassword());
		((BasicDataSource)mDataSource).setUrl(mDsAttr.getUrlString(m_Database));
		((BasicDataSource)mDataSource).setValidationQuery(validationQuery);
		((BasicDataSource)mDataSource).setTestWhileIdle(true);
	}

	
	/**
	 * 
	 * @return Connection
	 * 
	 * @throws Exception
	 * 
	 */
	public Connection getConnection()
	{
		try
		{
			return mDataSource.getConnection();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	
	/**
	 * @return String
	 */
	public String getDatabaseName()
	{
		return m_Database;
	}
	

	/**
	 * This method stops the connection pool
	 */
	public void stop()
	{
		BasicDataSource bds = (BasicDataSource) mDataSource;
		try
		{
			bds.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
}