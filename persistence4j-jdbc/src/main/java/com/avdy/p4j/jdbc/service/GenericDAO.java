/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.service;

import java.util.List;
import java.util.Map;

import com.avdy.p4j.common.DAOException;

/**
 * @author Avdhesh Yadav 
 */
public interface GenericDAO<E>
{
	//
	public int ALL_ROWS = -1;
	//
	public int initialPosition = 1;
	//
	public String ASC = "asc";
	//
	public String DESC = "desc";
	
	
	/**
	 * 
	 * @param entity Object
	 * 
	 * @return boolean
	 * 
	 * @throws DAOException
	 */
	boolean isEntityExists(E entity) throws DAOException;
	
	/**
	 * 
	 * @param valueObject Object
	 * 
	 * @return Object
	 * 
	 * @throws RemoteException
	 * @throws DAOException
	 */
	E createEntity(E valueObject) throws DAOException;
	
	/**
	 * This method first try to persist this entity.if this entity already exist 
	 * in the database then the database entity will be updated.
	 * 
	 * Note:- Method only works for the entities which have the primary key.
	 * 
	 * @param dataObject Object 
	 * 
	 * @throws DAOException
	 */
	void saveOrUpdate(E dataObject) throws DAOException;
	
	/**
	 * 
	 * @param valueObject Object
	 * 
	 * @throws RemoteException
	 * @throws DAOException
	 */
	void deleteEntity(E valueObject)throws DAOException;
	
	/**
	 * 
	 * @param vo Object
	 * 
	 * @param whereClause Map<String,Object>
	 * 
	 * @return Object
	 * @throws RemoteException
	 * @throws DAOException
	 */
	E updateEntity(E vo, Map<String,Object> whereClause) throws DAOException;
	
	/**
	 * 
	 * @param voClass Class 
	 * @param obj String
	 * 
	 * @return Object
	 * 
	 * @throws RemoteException
	 * @throws DAOException
	 */
	E findByPrimaryKey(Class voClass, Object[] obj)throws DAOException;
	
	/**
	 * 
	 * @param voClass Class
	 * @param param String
	 * @param value String
	 * @param orderByField String
	 * @param ascDesc String
	 * @param startPosition String
	 * @param rows int
	 * 
	 * @return List
	 * 
	 * @throws DAOException
	 */
	List<E> findByAttribute(Class voClass,String param, String value, String orderByField, String ascDesc,int startPosition, int rows)throws DAOException;
	
	/**
	 * 
	 * @param voClass Class
	 * @param params String[]
	 * @param values String[]
	 * @param orderByField String
	 * @param ascDesc String
	 * @param startPosition int
	 * @param rows int
	 * 
	 * @return List
	 * 
	 * @throws Exception
	 */
	List<E> find(Class voClass,String[] params,String values[],String orderByField,String ascDesc,int startPosition,int rows ) throws Exception;

	/**
	 * 
	 * @param tableName String
	 * @param query String
	 * @param startPosition int
	 * @param rows int 
	 * 
	 * @return List
	 * 
	 * @throws RemoteException
	 * @throws DAOException
	 */
	List<E> findByQuery(Class voClass , String query,int startPosition, int rows) throws DAOException;
	
	/**
	 *  
	 * @param voClass Class
	 * @param startPosition int
	 * @param rows int
	 * 
	 * @return List
	 * 
	 * @throws RemoteException
	 * @throws DAOException
	 */
	List<E> findAll(Class voClass, int startPosition , int rows)throws DAOException;
	
	
	/**
	 * 
	 * @param voClass Class
	 * @param params String
	 * @param value String
	 * 
	 * @return int
	 * 
	 * @throws DAOException
	 */
	 int countRows(Class voClass, String params, String value)throws DAOException;
	 
	 
	 
	 /**
	  * 
	  * @param objects List<Object>
	  * 
	  * @throws DAOException
	  */
	 void batchInsert(List<E> objects) throws DAOException;
	 
	 
	 /**
	  * 
	  * @param eoClass Class
	  * @param queries List<String>
	  * 
	  * @return List
	  * 
	  * @throws DAOException
	  */
	 List<E> executeQueries(Class eoClass , List<String> queries) throws DAOException;
	 
	 
	 /**
	  * 
	  * @param query String
	  * 
	  * @return boolean
	  * 
	  * @throws DAOException
	  */
	 boolean executeQuery(String query) throws DAOException;
}