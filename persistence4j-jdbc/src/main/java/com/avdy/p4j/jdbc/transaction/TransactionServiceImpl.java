/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.transaction;

import javax.transaction.UserTransaction;

import com.avdy.p4j.jdbc.service.TransactionService;

/**
 * @author Avdhesh Yadav - Avdhesh.Yadav@Gmail.com    
 */
public class TransactionServiceImpl implements TransactionService
{
	private static final long serialVersionUID = 1L;
	
	private TransactionManager _TM;

	public TransactionServiceImpl(UserTransaction ut) throws Exception
	{
		_TM = new TransactionManager(ut);
	}
	
	/**
	 * 
	 */
	public void beginTransaction()
	{
		try 
		{
			_TM.beginTransaction();
		} 
	    catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	
	/**
	 * 
	 */
	public void commitTransaction() 
	{
		try
		{
			_TM.commitTransaction();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	

	/**
	 * 
	 */
	public void rollbackTransaction()
	{
		try 
		{
			_TM.rollbackTransaction();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}