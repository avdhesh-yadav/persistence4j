/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dao;

/**
 * 
 * @author Avdhesh Yadav
 *
 */
public class QueryBuilder 
{
	public static String SELECT = "select * from ";

	public static String UPDATE = "update ";

	public static String DELETE = "delete from ";

	public static String WHERE = " where ";

	public static String AND = " and ";

	public static String OR = " or ";

	public static String EQUAL = " = ";

	public static String ORDERBY = " order by ";

	public static String ASC = " asc";

	public static String DESC = " desc";
	
	
	/**
	 * 
	 * @param tableName String
	 * @param queryType String
	 * @param params String[]
	 * @param values String[]
	 * @param orderByField String
	 * @param ascDesc String
	 * 
	 * @return String
	 */
	public String buildQuery(String tableName, String queryType, String[] params, String[] values, String orderByField, String ascDesc)
	{
		String query = "";
		String st = "";
		if (queryType.equals(DELETE))
		{
			query = query.concat(DELETE) + tableName + WHERE;
		}
		if (queryType.equals(SELECT))
		{
			query = query.concat(SELECT) + tableName + WHERE;
		}
		int paramLength = params.length;
		for (int i = 0; i < paramLength; i++)
		{
			if (i > 0)
			{
				st = st + " and " + params[i] + "='" + values[i] + "'";
			} else
			{
				st = st + params[i] + "='" + values[i] + "'";
			}
		}
		if (orderByField != null)
		{
			orderByField = orderByField.concat(" ");
			st = st + ORDERBY + orderByField + ascDesc;
		}
		query = query + st;
		return query;
	}
}
