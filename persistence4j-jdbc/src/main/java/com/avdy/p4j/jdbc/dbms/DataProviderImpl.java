/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms;

import com.avdy.p4j.jdbc.service.DataFetcher;
import com.avdy.p4j.jdbc.service.DataProvider;
import com.avdy.p4j.jdbc.service.TransactionService;

public class DataProviderImpl implements DataProvider
{
	/** */
	private String dbmsName;
	/** */
	private String databaseName;
	/** */
	private DataFetcher dataFetcher;
	
	private TransactionService transactionService;
	
	
	/**
	 * 
	 */
	public DataProviderImpl(String databaseName, String dbmsName, DataFetcher dataFetcher, TransactionService transactionService)
	{
		this.databaseName = databaseName;
		this.dbmsName = dbmsName;
		this.dataFetcher = dataFetcher;
		this.transactionService = transactionService;	
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getDatabaseName()
	{
		return databaseName;
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getDbms()
	{
		return dbmsName;
	}
	
	
	/**
	 * 
	 * @return DataFetcher
	 * 
	 */
	public DataFetcher getDataFetcher()
	{
		return dataFetcher;
	}
	
	
	/**
	 * 
	 * @return TransactionService
	 */
	public TransactionService getTransactionService()
	{
		if(transactionService != null)
			return transactionService;
		throw new IllegalStateException("TransactionService is null because database initilized with isTransactional boolean false.");
	}
}
