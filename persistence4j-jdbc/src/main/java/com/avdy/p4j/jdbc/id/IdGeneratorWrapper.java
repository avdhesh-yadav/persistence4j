/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.id;

/**
 * 
 * @author Avdhesh yadav
 */
public class IdGeneratorWrapper
{
	/** */
	private static IdGeneratorService mIdGeneratorService;
	/** */
	private static boolean mFreeze = false;
	
	
	/**
	 * 
	 * @param locator IServiceLocator
	 */
	public static synchronized void registerIdGenerator(IdGeneratorService idGeneratorService)
	{
		if(!mFreeze)
		{
			mIdGeneratorService = idGeneratorService;
			mFreeze = true;
		}
		else
		{
			throw new IllegalStateException("IdentifierGenerator is already register");
		}
	}
	
	
	/**
	 * 
	 * @return Object
	 * 
	 * @throws Exception
	 */
	public static Object nextIdentifier() throws Exception
	{
		return mIdGeneratorService.nextIdentifier();
	}
}