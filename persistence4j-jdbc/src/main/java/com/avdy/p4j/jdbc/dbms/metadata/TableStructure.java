/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms.metadata;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.avdheshyadav.spiderdb.dbmodel.Column;
import com.avdheshyadav.spiderdb.dbmodel.ForeignKey;
import com.avdheshyadav.spiderdb.dbmodel.Index;
import com.avdheshyadav.spiderdb.dbmodel.PrimaryKey;



/**
 * @author Avdhesh Yadav
 */
public class TableStructure
{
	//
	static Logger logger = Logger.getLogger(TableStructure.class.getName());
	//
	private final String mTableName;
	//
	private final List<Column> mColumns = new ArrayList<Column>(); 
	//
	private final PrimaryKey mPrimaryKey;
	//
	private ForeignKey[] mForeignKey;
	//
	private String mInsertQuery;
	//
	private String mUpdateQuery;
	//
	private Index[] mIndex;
	//
	private final String mSchemaName;
	
	
	/**
	 * 
	 * @param tableName String
	 * 
	 * @param primaryKey PrimaryKey
	 */
	public TableStructure(String schemaName , String tableName, PrimaryKey primaryKey ,Index[] indices)
	{
		mSchemaName = schemaName;
		mTableName  = tableName;
		mPrimaryKey = primaryKey;
		mIndex      = indices;
	}
	
	
	/**
	 * 
	 * @return PrimaryKey
	 */
	public PrimaryKey getPrimaryKey()
	{
		return mPrimaryKey;
	}
	
	
	/**
	 * 
	 * 
	 */
	public void setForeignKey(ForeignKey[] foreignKey)
	{
		mForeignKey = foreignKey;
	}
	
	
	/**
	 * 
	 * @return ForeignKey
	 */
	public ForeignKey[] getForeignKey()
	{
		return mForeignKey;
	}
	
	
	/**
	 * 
	 * @return Index[]
	 */
	public Index[] getIndices()
	{
		return mIndex;
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getSchemaName()
	{
		return mSchemaName;
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return mTableName;
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getFullTableName()
	{
		return mSchemaName + "." + mTableName;
	}

	
	/**
	 * 
	 * @param column Column
	 */
	public void addColumn(Column column)
	{
		mColumns.add(column);
	}

	
	/**
	 * 
	 * @return  List<Column>
	 */
	public List<Column> getColumns()
	{
		return mColumns;
	}

	
	/**
	 * 
	 */
	protected void createQueries()
	{
		createInsertQuery();
		creatUpdateQuery();
	}
	
	
	/**
	 * 
	 */
	private void createInsertQuery()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("insert into ");
		sb.append(getFullTableName());
		sb.append(" values ( ? ");
		for (int i = 1; i < mColumns.size(); i++)
		{
			sb.append(", ? ");
		}
		sb.append(")");
		mInsertQuery = sb.toString();
		logger.debug("insert Query :" + mInsertQuery);
	}
	
	
	/**
	 * 
	 */
	private void creatUpdateQuery()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("update ");
		sb.append(getFullTableName());
		sb.append(" set ");
		
		for (Column column : mColumns)
		{
			sb.append(column.getColumnName()+ " = ? ," );
		}
		
		StringBuffer sb1 =new StringBuffer(sb.substring(0, sb.length()-1));
		
		//logger.debug("last index Query Query :" + str);
		sb1.append(" where ");
		
		mUpdateQuery = sb1.toString();
		logger.debug("update Query Query :" + mUpdateQuery);
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getInsertQuery()
	{
		return mInsertQuery;
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getUpdateQuery()
	{
		return mUpdateQuery;
	}
}