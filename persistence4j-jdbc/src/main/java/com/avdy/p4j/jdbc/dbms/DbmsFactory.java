/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms;

import com.avdy.p4j.common.DbmsType;
import com.avdy.p4j.jdbc.PersistenceConfig;


/**
 * @author Avdhesh Yadav - Avdhesh.Yadav@Gmail.com    
 */
public class DbmsFactory 
{

	/**
	 * 
	 * @param dbmsName String
	 * @param props PersistenceConfig
	 * 
	 * @return Dbms
	 */
	public static Dbms getDBMS(String dbmsName, PersistenceConfig mConfig)
	{
		Dbms dbms = null;
		
		if(dbmsName.equalsIgnoreCase(DbmsType.MYSQL))
		{
			dbms = MySqlDbms.getInstance(mConfig);
		}
		else if(dbmsName.equalsIgnoreCase(DbmsType.PGSQL))
		{
			dbms = PgSqlDbms.getInstance(mConfig);
		}
		else if(dbmsName.equalsIgnoreCase(DbmsType.DERBY))
		{
			dbms = DerbyDbms.getInstance(mConfig);
		}
		else if(dbmsName.equalsIgnoreCase(DbmsType.HSQLDB))
		{
			dbms = HsqlDbms.getInstance(mConfig);
		}
		return dbms;
	}
}
