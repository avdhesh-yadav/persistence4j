/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms.connfactory.xa.providers;

import javax.transaction.UserTransaction;

import com.avdy.p4j.jdbc.dbms.DataSourceAttr;
import com.avdy.p4j.jdbc.dbms.connfactory.xa.XAProvider;

/**
 * 
 * @author Avdhesh Yadav
 *
 */
abstract class AbsXaProviderImpl implements XAProvider
{
	/** */
	protected DataSourceAttr mDsAttr;
	//
	protected String mDatabase;
	//
	protected String mTestQuery = "select 1";
	//
	protected UserTransaction mUserTransaction = null;
	
	
	/**
	 * 
	 * @param dsAttr DataSourceAttr
	 * 
	 * @throws Exception
	 */
	public AbsXaProviderImpl(String database,DataSourceAttr dsAttr) throws Exception
	{
		mDatabase = database;
		mDsAttr = dsAttr;
	}
	
	
	/**
	 * 
	 *  @return UserTransaction
	 */
	public UserTransaction getXAUserTransaction()
	{
		return mUserTransaction;
	}
}
