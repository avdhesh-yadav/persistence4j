/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms;

import org.apache.log4j.Logger;

import com.avdy.p4j.common.DbmsType;

/**
 * @author Avdhesh Yadav - Avdhesh.Yadav@Gmail.com    
 */
public class DataSourceAttr 
{
	private static Logger logger = Logger.getLogger(DataSourceAttr.class.getName());
	/** */
	public String MYSQL_PREFIX = "jdbc:mysql://";
	/** */
	public String PGSQL_PREFIX = "jdbc:postgresql://";
	/** */
	public String DERBY_PREFIX = "jdbc:derby:";
	/** */
	public String HSQL_PREFIX = "jdbc:hsqldb:file:";
	/** */
	public static final String DEF_MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	/** */
	public static final String DEF_HOST = "localhost";
	/** */
	public static final String DEF_PGSQL_DRIVER = "org.postgresql.Driver";
	/** */
	protected String m_Driver = DEF_MYSQL_DRIVER;
	/**Derby Embedded Driver */
	public static final String DEF_DERBY_EMBEDDED_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	/**HSQLDB Embedded Driver */
	public static String HSQLDB_DRIVER = "org.hsqldb.jdbc.JDBCDriver";
	/** */
	protected String m_HostName = DEF_HOST;
	/** */
	protected String m_Username;
	/** */
	protected String m_Password;
	/** */
	protected int m_MinPool= 2;
	/** */
	protected int m_MaxPool= 10;
	/** */
	private String m_UrlString;
	/** */
	public static final int DEF_MYSQL_PORT = 3306;
	/** */
	public static final int DEF_PGSQL_PORT = 5432;
	/** */
	private int m_Port = DEF_MYSQL_PORT;
	/** */
	private String m_XaDataSourceClassName;
	/** */
	private boolean mAutoReconnect = false;
	
	
	/**
	 * 
	 * @param driver String
	 * @param hostName String
	 * @param port int
	 * @param schema String
	 * @param userName String
	 * @param password String
	 * @param xaDsClassName String
	 */
	public DataSourceAttr(String driver,String hostName, int port,String userName, String password,String xaDsClassName)
	{
		m_Driver = driver;
		m_HostName = hostName;
		m_Username = userName;
		m_Password = password;
		m_Port = port;
		m_XaDataSourceClassName = xaDsClassName;
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getDriverClassName()
	{
		return m_Driver;
	}
	
	
	/**
	 * 
	 */
	public void setAutoReconnect()
	{
		mAutoReconnect = true;
	}
	
	
	/**
	 * 
	 * example jdbc:mysql://" for mysql and "jdbc:postgresql://" for pgsql
	 * @return String
	 */
	public String getUrlString(String database)
	{
		System.out.println("mDriver:"+m_Driver);
		String driverPrfix = "";
		if(m_Driver.contains(DbmsType.MYSQL))
		{
			driverPrfix = MYSQL_PREFIX;
		}
		else if(m_Driver.contains("postgresql"))
		{
			driverPrfix = PGSQL_PREFIX;
		}
		else if(m_Driver.contains(DbmsType.DERBY))
		{
			driverPrfix = DERBY_PREFIX;
		}
		else if(m_Driver.contains(DbmsType.HSQLDB))
		{
			driverPrfix = HSQL_PREFIX;
		}
		
		
		//Special case for handling of Apache Derby
		if(driverPrfix.contains(DbmsType.DERBY))
		{
			m_UrlString = driverPrfix + database + ";create=true";
		}
		else if(driverPrfix.contains(DbmsType.HSQLDB))
		{
			m_UrlString = driverPrfix + database;
		}
		// This is the case only for MySQL and Postgres
		else if(driverPrfix.contains(DbmsType.MYSQL) || driverPrfix.contains("postgresql"))
		{
			m_UrlString = driverPrfix + m_HostName + ":" + m_Port + "/" + database;
			// if auto reconnect is true the enable thi feature
			if(mAutoReconnect)
			{
				m_UrlString = m_UrlString + "?autoReconnect=true";
			}
		}
		logger.info("m_UrlString:"+m_UrlString);
		return m_UrlString;
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getUserName()
	{
		return m_Username;
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getPassword()
	{
		return m_Password;
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getDatabaseHost()
	{
		return m_HostName;
	}
	
	/**
	 * 
	 * @return int
	 */
	public int getPort()
	{
		return m_Port;
	}
	
	
	/**
	 * 
	 * @return int
	 */
	public int getMinPoolSize()
	{
		return m_MinPool;
	}
	
	
	public void setMinPoolSize(int size)
	{
		m_MinPool = size;
	}
	
	
	/**
	 * 
	 * @return int
	 */
	public int getMaxPoolSize()
	{
		return m_MaxPool;
	}
	
	
	/**
	 * 
	 * @param size int
	 */
	public void setMaxPoolSize(int size)
	{
		m_MaxPool = size;
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getXaDataSourceClassName()
	{
		return m_XaDataSourceClassName;
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getDbmsType()
	{
		String type = "";
	
		if(m_Driver.contains(DbmsType.MYSQL))
		{
			type = DbmsType.MYSQL;
		}
		else if(m_Driver.contains("postgresql"))
		{
			type = DbmsType.PGSQL;
		}
		else if(m_Driver.contains(DbmsType.DERBY))
		{
			type = DbmsType.DERBY;
		}
		else if(m_Driver.contains(DbmsType.HSQLDB))
		{
			type = DbmsType.HSQLDB;
		}
		return type;
	}
	
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		System.out.println("DataSourceAttr :>"+m_Driver );
		System.out.println(" m_Driver :>"+m_Driver );
		System.out.println(" m_HostName :>"+m_HostName );
		System.out.println(" m_Username :>"+m_Username );
		System.out.println(" m_Password :>"+ m_Password );
		System.out.println(" m_MinPool :>"+m_MinPool );
		System.out.println(" m_MaxPool :>"+m_MaxPool );
		System.out.println(" m_XaDataSourceClassName :>"+m_XaDataSourceClassName );
		return sb.toString();
	}
}