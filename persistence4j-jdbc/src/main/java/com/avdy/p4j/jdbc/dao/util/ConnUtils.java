/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 
 * @author Avdhesh Yadav
 *
 */
public final class ConnUtils 
{
	/**
	 * 
	 * @param conn Connection Connection
	 * @param stmt Statement Statement
	 * @param rs ResultSet ResultSet
	 */
	public static void closeQuietly(Connection conn, Statement stmt, ResultSet rs) 
	{
		try 
		{
			closeResultQuietly(rs);
		} 
		finally 
		{
			try
			{
				closeStatementQuietly(stmt);
			}
			finally 
			{
				closeConnectionQuietly(conn);
			}
		}
	}


	/**
	 * 
	 * @param conn Connection
	 */
	public static void closeConnectionQuietly(Connection conn)
	{
		try 
		{
			closeConnection(conn);
		} 
		catch (SQLException e)
		{

		}
	}


	/**
	 * 
	 * @param stmt Statement
	 */
	public static void closeStatementQuietly(Statement stmt) 
	{
		try
		{
			closeStatement(stmt);
		} 
		catch (SQLException e) 
		{

		}
	}


	/**
	 * 
	 * @param rs ResultSet
	 */
	public static void closeResultQuietly(ResultSet rs) 
	{
		try
		{
			closeResult(rs);
		} 
		catch (SQLException e) 
		{

		}
	}


	/**
	 * 
	 * @param conn Connection
	 * 
	 * @throws SQLException
	 */
	public static void closeConnection(Connection conn) throws SQLException
	{
		if (conn != null)
		{
			conn.close();
		}
	}


	/**
	 * 
	 * @param stmt Statement
	 * 
	 * @throws SQLException
	 */
	public static void closeStatement(Statement stmt) throws SQLException
	{
		if (stmt != null) 
		{
			stmt.close();
		}
	}


	/**
	 * 
	 * @param rs ResultSet
	 * 
	 * @throws SQLException
	 */
	public static void closeResult(ResultSet rs) throws SQLException
	{
		if (rs != null)
		{
			rs.close();
		}
	}
}
