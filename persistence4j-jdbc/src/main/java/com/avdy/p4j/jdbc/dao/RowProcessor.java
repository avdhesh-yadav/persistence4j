/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.avdy.p4j.jdbc.dbms.metadata.TableStructure;
import com.avdy.p4j.jdbc.model.DTO;

/**
 * 
 * @author Avdhesh Yadav
 *
 */
public interface RowProcessor 
{
	/**
	 * @param tableStructure TableStructure
	 * @param rs ResultSet
	 * @param startPosition startPosition
	 * @param rows startPosition
	 * 
	 * @return List<?>
	 * 
	 * @throws SQLException
	 */
    public List<?> toBeanList(TableStructure tableStructure, ResultSet rs, int startPosition, int rows) throws SQLException;
    
    /** 
     * 
     * @param tableStructure TableStructure
     * @param dto DTO
     * @param pstmt PreparedStatement
     * 
     * @throws SQLException
     */
    public void copyEntityObjectToPstmt(TableStructure tableStructure, DTO dto, PreparedStatement pstmt)throws SQLException;
}