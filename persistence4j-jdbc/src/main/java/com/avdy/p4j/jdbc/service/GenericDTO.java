/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.service;

import java.util.Map;

import com.avdy.p4j.jdbc.model.DTO;

/**
 * 
 * @author Avdhesh yadav
 */
public class GenericDTO extends DTO
{
	private static final long serialVersionUID = 1L;
	
	private String mTableName = null;
	
	private Map<String, Object> mData = null;
	
	
	/**
	 * 
	 * @param tableName String
	 * 
	 * @param data Map<String, Object>
	 */
	public GenericDTO(String tableName , Map<String, Object> data)
	{
		mTableName = tableName;
		mData = data;
	}
	
	
	/**
	 * 
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getData()
	{
		return mData;
	}
	
	
	/**
	 * 
	 * @param columnName String
	 * 
	 * @return Object
	 */
	public Object getColumnValue(String colName)
	{
		return mData.get(colName);
	}
	
	
	/**
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return mTableName;
	}
}