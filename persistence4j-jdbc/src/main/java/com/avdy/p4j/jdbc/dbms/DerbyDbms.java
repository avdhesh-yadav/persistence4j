/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms;

import org.apache.log4j.Logger;

import com.avdy.p4j.common.DbmsType;
import com.avdy.p4j.jdbc.PersistenceConfig;
import com.avdy.p4j.jdbc.common.CRUD;
import com.avdy.p4j.jdbc.common.GenericCRUD;
import com.avdy.p4j.jdbc.dao.AbsPersistenceManager;
import com.avdy.p4j.jdbc.dao.DefaultPersistenceManager;
import com.avdy.p4j.jdbc.dbms.connfactory.ConnectionFactory;
import com.avdy.p4j.jdbc.dbms.connfactory.DBConnector;
import com.avdy.p4j.jdbc.dbms.metadata.IMetaDataLoader;
import com.avdy.p4j.jdbc.dbms.metadata.MetaDataLoader;

/**
 * @author Avdhesh Yadav - Avdhesh.Yadav@Gmail.com    
 */
class DerbyDbms extends Dbms
{
	static Logger logger = Logger.getLogger(DerbyDbms.class.getName());
	
	private static Dbms _Instance;

	/**
	 * 
	 * @param props PersistenceConfig
	 * 
	 * @return Dbms
	 */
	public static Dbms getInstance(PersistenceConfig config)
	{
		if(_Instance == null)
		{
			_Instance = new DerbyDbms(config);
		}
		return _Instance;
	}
	
	
	/**
	 * 
	 * @param config PersistenceConfig
	 */
	private DerbyDbms(PersistenceConfig config)
	{
		super(config);
		m_ConnFactory = ConnectionFactory.getConnectionFactory(DbmsType.DERBY);
		mDsAttr = constructDataSourceAttr(DbmsType.DERBY);
	}
	
	
	/**
	 * 
	 * @param database String
	 * 
	 * @throws Exception
	 */
	public void createDataSource(String database, boolean isTransactional) throws Exception
	{
		logger.info("createDataSource in derby:"+database);
		
		boolean isNew = isNewDataSource(database);
		
		if(isNew)
		{
			DBConnector connector = getDBConnector(database, isTransactional);
			IMetaDataLoader loader = new MetaDataLoader(connector);
			CRUD crud = new GenericCRUD();
			AbsPersistenceManager pcManager = new DefaultPersistenceManager(loader, crud);
			m_PcManagerMap.put(database, pcManager);
		}
	}
}
