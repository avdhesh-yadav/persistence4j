/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms.connfactory.xa.providers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.XADataSource;

import org.apache.log4j.Logger;

import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.jdbc.AtomikosDataSourceBean;
import com.avdy.p4j.jdbc.dbms.DataSourceAttr;
import com.avdy.p4j.jdbc.dbms.connfactory.xa.XAProvider;

/**
 * 
 * @author Avdhesh Yadav
 *
 */
class AtomikosXAProviderimpl extends AbsXaProviderImpl implements XAProvider
{
	static Logger logger = Logger.getLogger(AtomikosXAProviderimpl.class.getName());
	/** */
	private AtomikosDataSourceBean mAtomikosDataSourceBean;


	/**
	 * 
	 * @param schema database
	 * @param dsAttr DataSourceAttr
	 * 
	 * @throws Exception
	 */
	public AtomikosXAProviderimpl(String database, DataSourceAttr dsAttr) throws Exception
	{
		super(database, dsAttr);
		initilizeDatasource();
	}
	
	
	/**
	 * 
	 * @param dsAttr DataSourceAttr
	 * 
	 * @throws Exception
	 */
	private void initilizeDatasource() throws Exception
	{
		logger.info("initilizeDataSource mDatabase:"+mDatabase + "dsAttr.getDbmsType():" + mDsAttr.getDbmsType());
		mAtomikosDataSourceBean = new AtomikosDataSourceBean();
		String resourceName = mDsAttr.getDbmsType() +"/" + mDatabase;
		mAtomikosDataSourceBean.setUniqueResourceName(resourceName);
		mAtomikosDataSourceBean.setXaDataSourceClassName(mDsAttr.getXaDataSourceClassName());
		
		Properties props = new Properties();
		props.setProperty ( "user" , mDsAttr.getUserName() );
		props.setProperty ( "password" , mDsAttr.getPassword() );
		props.setProperty ( "serverName" , mDsAttr.getDatabaseHost());
		props.setProperty ( "portNumber" , String.valueOf(mDsAttr.getPort()));
		props.setProperty ( "databaseName" , mDatabase);
		
		mAtomikosDataSourceBean.setXaProperties(props);
		mAtomikosDataSourceBean.setMinPoolSize(mDsAttr.getMinPoolSize());
		mAtomikosDataSourceBean.setMaxPoolSize(mDsAttr.getMaxPoolSize());
		mAtomikosDataSourceBean.setTestQuery(mTestQuery);
		mAtomikosDataSourceBean.setDefaultIsolationLevel(Connection.TRANSACTION_READ_COMMITTED);
		
		mUserTransaction = new UserTransactionImp();
		mUserTransaction.setTransactionTimeout(60);
	}	
	
	
	/**
	 * 
	 *  @return XADataSource
	 */
	public XADataSource getXaDataSource()throws SQLException
	{
		return mAtomikosDataSourceBean.getXaDataSource();
	}
	
	
	/**
	 * 
	 * @return Connection
	 */
	public Connection getConnection()throws SQLException
	{
		return mAtomikosDataSourceBean.getConnection();
	}
	
	
	/**
	 * 
	 */
	public void stop()
	{
		mAtomikosDataSourceBean.close();
	}
}
