/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class is Thread Safe.
 * 
 *  @author Avdhesh Yadav  
 *
 */
public class GenericCRUD implements CRUD
{
	/**
	 * 
	 */
	public GenericCRUD()
	{
		
	}
	
	
	/**
	 * 
	 * @param pstmt PreparedStatement
	 * 
	 * @return boolean
	 * 
	 * @throws SQLException
	 */
	public boolean create(PreparedStatement pstmt)throws SQLException
	{
		int i = pstmt.executeUpdate();
		return  (i== 0) ? false : true;
	}

	/**
	 * 
	 * @param pstmt PreparedStatement
	 * 
	 * @return boolean
	 * 
	 * @throws SQLException
	 */
	public int[] batchInsert(PreparedStatement pstmt)throws SQLException
	{
		return pstmt.executeBatch();
	}
	
	
	/**
	 * 
	 * @param pstmt PreparedStatement
	 * 
	 * @return ResultSet
	 * 
	 * @throws SQLException
	 */
	public ResultSet read(PreparedStatement pstmt)throws SQLException
	{
		return pstmt.executeQuery();	
	}

	
	/**
	 * 
	 * @param pstmt PreparedStatement
	 * 
	 * @return boolean
	 * 
	 * @throws SQLException
	 */
	public boolean update(PreparedStatement pstmt)throws SQLException
	{
		int i = pstmt.executeUpdate();
		return  (i== 0) ? false : true;
	}

	
	/**
	 * 
	 * @param conn Connection
	 * @param query String
	 * 
	 * @return boolean
	 * 
	 * @throws SQLException
	 */
	public	boolean delete(Connection conn, String query)throws SQLException
	{
		PreparedStatement pstmt = conn.prepareStatement(query);
		int i = pstmt.executeUpdate();
		pstmt.close();
		return  (i== 0) ? false : true;
	}
}