/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import com.avdy.p4j.common.DAOException;
import com.avdy.p4j.jdbc.PersistenceConfig;
import com.avdy.p4j.jdbc.dao.AbsPersistenceManager;
import com.avdy.p4j.jdbc.dbms.connfactory.ConnectionFactory;
import com.avdy.p4j.jdbc.dbms.connfactory.DBConnector;
import com.avdy.p4j.jdbc.dbms.connfactory.XaDBConnector;
import com.avdy.p4j.jdbc.dbms.metadata.IMetaDataLoader;


/**
 * @author Avdhesh Yadav   
 */
abstract class Dbms 
{
	static Logger logger = Logger.getLogger(Dbms.class.getName());
	
	protected String m_XaApiProvider;
	
	protected PersistenceConfig mConfig;

	protected Map<String, AbsPersistenceManager> m_PcManagerMap;
	
	protected ConnectionFactory m_ConnFactory;
	
	protected DataSourceAttr mDsAttr;
	
	/**
	 * 
	 */
	public Dbms(PersistenceConfig config)
	{
		mConfig = config;
		m_PcManagerMap = new HashMap<String, AbsPersistenceManager>();
		m_XaApiProvider = mConfig.getXaApiProvider();
	}


	/**
	 *  
	 * @param dbms String
	 * 
	 * @return DataSourceAttr
	 */
	public DataSourceAttr constructDataSourceAttr(String dbms)
	{
		String driver   = mConfig.getDatabaseDriver(dbms);
		String hostName = mConfig.getHostName(dbms);
		int port 	   = mConfig.getDataBasePort(dbms);
		String username = mConfig.getUserName(dbms);
		String password = mConfig.getPassword(dbms);
		int minPool  = mConfig.getMinPool(dbms);
		int maxPool  = mConfig.getMaxPool(dbms);
		String xaDataSourceClassName = mConfig.getXaDataSourceClassName(dbms);
		mDsAttr = new DataSourceAttr(driver,hostName,port,username,password,xaDataSourceClassName);
		mDsAttr.setMinPoolSize(minPool);
		mDsAttr.setMinPoolSize(maxPool);
		return mDsAttr;
	}
	
	
	/**
	 * 	
	 * @param database String
	 * 
	 * @return AbsPersistenceManager String
	 * 
	 * @throws Exception
	 */
	public  AbsPersistenceManager getPCManager(String database) throws DAOException
	{
		if(m_PcManagerMap.containsKey(database))
		{
			return  m_PcManagerMap.get(database);
		}
		else
		{
			throw new DAOException("DataSource is not Found:"+database);
		}
	}
	
	
	/**
	 * 
	 * @param database String
	 * 
	 * @return UserTransaction
	 * 
	 * @throws Exception
	 */
	public UserTransaction getTMService(String database, final boolean isTransactional) throws DAOException
	{
		UserTransaction ut = null;
		AbsPersistenceManager pcManager = m_PcManagerMap.get(database);
		if(pcManager != null)
		{
			if(isTransactional)
			{
				IMetaDataLoader loader = pcManager.getMetaDataLoader();
				XaDBConnector connector = (XaDBConnector)loader.getDBConnector();
				ut = connector.getUserTransaction();
			}
		}
		else
		{
			String errMsg = "This database is not found:" + database;
			logger.info(errMsg);
			throw new DAOException(errMsg);
		}
		return ut;
	}
	
	
	/**
	 * 
	 * @param database String
	 * 
	 * @throws Exception
	 */
	public abstract void createDataSource(String database, final boolean isTransactional) throws Exception;

	
	/**
	 * 
	 * @return List
	 */
	public List<String> listDataSources()
	{
		List<String> datasources = new ArrayList<String>();
		
		Set<String> keys = m_PcManagerMap.keySet();
		
		for(String database : keys)
		{
			datasources.add(database);
		}
		return datasources;
	}
	
	
	/**
	 * 
	 * @param database String
	 * 
	 * @throws DAOException
	 */
	public void unBindDatasource(String database)throws DAOException
	{
		logger.info("undindDatasource:"+database);
		AbsPersistenceManager pcManager = m_PcManagerMap.get(database);

		if(pcManager != null)
		{
			DBConnector connector = pcManager.getDBConnector();
			if(connector != null)
				connector.stop();
		}
		else
		{
			throw new DAOException("database Does not Exist.:" + database);
		}
		
		m_PcManagerMap.remove(database);
		logger.info(database + " database unbinded sucessfully");
	}

	
	/**
	 * 
	 * @param database String
	 * 
	 * @return boolean
	 * 
	 */
	protected boolean isNewDataSource(String database)
	{
		boolean isNew = true;
		System.out.println("m_PcManagerMap:"+m_PcManagerMap);
		if(m_PcManagerMap.containsKey(database))
		{
			isNew = false;
		}
		return isNew;
	}
	
	
	/**
	 * here we test weather the Database connector is transactional or non-transactional
	 * @param database String
	 * 
	 * @return DBConnector
	 * 
	 * @throws Exception
	 */
	protected DBConnector getDBConnector(String database, boolean isTransactional) throws Exception
	{
		DBConnector connector = null;
		//here we test weather the Database connector is transactional or non-transactional
		if(isTransactional)
		{
			connector = m_ConnFactory.newXADBConnector(m_XaApiProvider, database, mDsAttr);
		}
		else
		{
			connector = m_ConnFactory.newNonXADBConnector(database, mDsAttr);
		}
		return connector;
	}
}