/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.dbms.metadata;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Set;

import javax.transaction.UserTransaction;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.avdheshyadav.spiderdb.dbmodel.Column;
import com.avdheshyadav.spiderdb.dbmodel.PrimaryKey;
import com.avdy.p4j.jdbc.dbms.DataSourceAttr;
import com.avdy.p4j.jdbc.dbms.connfactory.DBConnector;

public class MetaDataLoaderTest 
{
	private static String databaseName = "myDerby";
	private static String userName = "library";
	private static String password = "library";
	private static DBConnector dbConnector;
	
	@BeforeClass
	public static void setUp() throws Exception
	{
		DataSourceAttr dataSourceAttr = new DataSourceAttr(DataSourceAttr.DEF_DERBY_EMBEDDED_DRIVER, DataSourceAttr.DEF_HOST, 0, userName, password, "org.apache.derby.jdbc.EmbeddedXADataSource");
		dbConnector = new TestDBConnector(databaseName,dataSourceAttr);
	}


	@AfterClass
	public static void tearDown() throws Exception
	{
		dbConnector.stop();
	}
	
	
	@Test
	public void testGetTableStructure() 
	{
		try 
		{
			IMetaDataLoader metaDataLoader = new MetaDataLoader(dbConnector);
			String schemaName = "library";
			String tableName = "book";
			TableStructure tableStructure =  metaDataLoader.getTableStructure(schemaName, tableName);
			
			Assert.assertNotNull(tableStructure);
			Assert.assertEquals("library", tableStructure.getSchemaName());
			Assert.assertEquals("book", tableStructure.getTableName());
			Assert.assertEquals("library.book", tableStructure.getFullTableName());
			
			System.out.println("SchemaName:"+tableStructure.getSchemaName()+ "TableName:"+ tableStructure.getTableName() + " FullTableName:"+tableStructure.getFullTableName());
			
			List<Column> columns = tableStructure.getColumns();
			Assert.assertTrue(columns.size() > 0);
		
			for(Column column : columns)
			{
				System.out.println("column:"+column);
			}			
			
			PrimaryKey primaryKey = tableStructure.getPrimaryKey();
			System.out.println("primaryKey.getPkName():"+primaryKey.getPkName());
			Set<Column> primaryColumns = primaryKey.getColumns();
			for(Column priColumn : primaryColumns)
			{
				System.out.println("priColumn:"+priColumn);
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			Assert.fail("testGetTableStructure failed");
		}
	}
	
	
	/**
	 * 
	 * @author Avdhesh Yadav
	 *
	 */
	static class TestDBConnector implements DBConnector
	{
		private String databaseName ;
		private DataSourceAttr dataSourceAttr;
		
		/**
		 * 
		 * @param databaseName String
		 * @param dataSourceAttr DataSourceAttr
		 */
		public TestDBConnector(String databaseName, DataSourceAttr dataSourceAttr)
		{
			this.dataSourceAttr = dataSourceAttr;
			this.databaseName = databaseName;
		}
		
		
		/**
		 * 
		 * @return Connection
		 * 
		 * @throws Exception
		 */
		public Connection getConnection()
		{
			try 
			{
				Class.forName(dataSourceAttr.getDriverClassName());
				System.out.println("URL:"+dataSourceAttr.getUrlString(databaseName));
				Connection connection = DriverManager.getConnection(dataSourceAttr.getUrlString(databaseName), dataSourceAttr.getUserName(), dataSourceAttr.getPassword());
				createTables(connection);
				return connection;
			
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				throw new RuntimeException("Sorry Connection can not be made:"+e.getMessage());
			}
		}
		
		
		/**
		 * 
		 * @return UserTransaction
		 */
		public UserTransaction getUserTransaction()
		{
			return null;
		}
		
		
		/**
		 * 
		 * @return String
		 */
		public String getDatabaseName()
		{
			return databaseName;
		}
		
		
		/**
		 * 
		 */
		public void stop()
		{
			
		}
		
		/**
		 * 
		 */
		public static void createTables(Connection connection) throws SQLException
		{
			Statement s = connection.createStatement();
			try {
				String book_drop = "DROP TABLE book";
				String book_issue_drop = "DROP TABLE book_issue";
				String book_inventory_drop = "DROP TABLE book_inventory";
				String student_drop = "DROP TABLE student";
				String author_drop = "DROP TABLE author";
				s.execute(book_drop);
				s.execute(book_issue_drop);
				s.execute(book_inventory_drop);
				s.execute(student_drop);
				s.execute(author_drop);
			} catch (SQLException e) {

			}

			// create book table
			String book = "CREATE TABLE book (isbn VARCHAR(45) NOT NULL, title VARCHAR(128) NOT NULL, authorid INT NOT NULL, PRIMARY KEY (isbn))";
			s.execute(book);

			// create book_issue table
			String book_issue = "CREATE TABLE book_issue (isbn VARCHAR(45) NOT NULL, rollno INT NOT NULL, date_of_issue TIMESTAMP NOT NULL,date_of_return TIMESTAMP DEFAULT NULL, PRIMARY KEY (isbn))";
			s.execute(book_issue);

			// create book_inventory table
			String book_inventory = "CREATE TABLE book_inventory (isbn VARCHAR(45) NOT NULL, no_available INT NOT NULL, totalbooks INT NOT NULL, PRIMARY KEY (isbn))";
			s.execute(book_inventory);


			// create student table
			String student = "CREATE TABLE student (rollno INT NOT NULL, name VARCHAR(45) NOT NULL, PRIMARY KEY (rollno))";
			s.execute(student);

			// create author table
			String author = "CREATE TABLE author (authorid INT NOT NULL, author_name VARCHAR(45) NOT NULL, PRIMARY KEY (authorid))";
			s.execute(author);

			connection.commit();
		}
	}
}