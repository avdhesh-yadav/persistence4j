/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.jdbc.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import com.avdy.p4j.common.DAOException;
import com.avdy.p4j.jdbc.PersistenceConfig;
import com.avdy.p4j.jdbc.dbms.DataProviderFactoryImpl;
import com.avdy.p4j.jdbc.transfer.TransferUtil;


/**
 * 
 * @author Avdhesh Yadav
 *
 */
public class GenericDaoTest
{
	private static GenericDAO<Book> genericDAO;

	@BeforeClass
	public static void setUp() throws Throwable
	{
		Properties props = new Properties();
		ClassLoader loader = GenericDaoTest.class.getClassLoader();
		InputStream inStream = loader.getSystemResourceAsStream("persistence.properties");
		props.load(inStream);
		PersistenceConfig config = new PersistenceConfig(props);
		DerbySampleDataLoader sampleDataLoader = new DerbySampleDataLoader("library", "derby", config);
		sampleDataLoader.createLibraryTables();
		sampleDataLoader.insertRows();

		DataProviderFactory dataProviderFactory = new DataProviderFactoryImpl(config);
		DataFetcher dataFetcher = dataProviderFactory.getDataProvider("library", "derby", false).getDataFetcher();

		TransferUtil.registerClass(Book.class);

		genericDAO = new GenericDaoImpl<Book>(dataFetcher);
	}


	@Test
	public void isEntityExists() throws DAOException
	{
		Book book = new Book("81-203-0855-7", "Computer System Architecture", 4);
		boolean exists = genericDAO.isEntityExists(book);
		Assert.assertTrue(exists);
	}


	@Test
	public void createEntity() throws DAOException
	{
		Book newBook = new Book("1-56592-485-1", "Java I/O", 6);
		genericDAO.createEntity(newBook);
	}


	@Test
	public void saveOrUpdate()throws DAOException
	{
		Book testBook = new Book("testSaveOrUpdate", "Java I/O", 6);
		//It should create New book Here
		genericDAO.saveOrUpdate(testBook);
		// now lets update the title of the book
		testBook.setTitle("Java IO");
		genericDAO.saveOrUpdate(testBook);

		Object []obj = new Object[1]; 
		obj[0] = "testSaveOrUpdate";
		Book updatedBook = 	genericDAO.findByPrimaryKey(Book.class, obj);
		Assert.assertEquals(testBook.getTitle(), updatedBook.getTitle());
	}


	@Test
	public void deleteEntity()throws DAOException
	{
		Book testBook = new Book("testDeleteBook", "Java I/O", 6);
		genericDAO.createEntity(testBook);

		genericDAO.deleteEntity(testBook);
	}


	@Test
	public void updateEntity() throws DAOException
	{
		Book testBook = new Book("testBook", "Java I/O", 6);
		genericDAO.createEntity(testBook);
		Map<String, Object> whereClause = new HashMap<String, Object>();
		whereClause.put("isbn", "testBook");
		testBook.setTitle("Java IO");
		genericDAO.updateEntity(testBook, whereClause);
		Object[] obj = new Object[1];
		obj[0] = "testBook";
		Book updatedBook = genericDAO.findByPrimaryKey(Book.class, obj);
		// Test whether book title is updates
		Assert.assertEquals(testBook.getTitle(), updatedBook.getTitle());
	}


	@Test
	public void findByPrimaryKey() throws DAOException
	{
		Object[] obj = new Object[1];
		obj[0] = "81-7366-270-3";
		Book book = genericDAO.findByPrimaryKey(Book.class, obj);
		Assert.assertNotNull(book);
	}

	@Test
	public void findByAttribute()throws DAOException
	{
		List<Book> books= genericDAO.findByAttribute(Book.class, "title", "Computer System Architecture" , null, null, GenericDAO.initialPosition, GenericDAO.ALL_ROWS);
		Assert.assertEquals(1, books.size());
	}


	@Test
	public void findAll()throws DAOException
	{
		List<Book> books= genericDAO.findAll(Book.class, GenericDAO.initialPosition, 5);
		Assert.assertEquals(5, books.size());
	}


	@Test
	public void countRows() throws DAOException
	{
		int rows = genericDAO.countRows(Book.class, "title", "Computer System Architecture");
		Assert.assertEquals(1, rows);
	}


	@Test
	public void batchInsert() throws DAOException
	{
		Book book1 = new Book("book1", "book1", 1);
		Book book2 = new Book("book2", "book2", 1);
		Book book3 = new Book("book3", "book3", 1);
		List<Book> books = new ArrayList<Book>();
		books.add(book1);
		books.add(book2);
		books.add(book3);
		genericDAO.batchInsert(books);

		Assert.assertTrue(genericDAO.isEntityExists(book1));
		Assert.assertTrue(genericDAO.isEntityExists(book2));
		Assert.assertTrue(genericDAO.isEntityExists(book3));
	}
}
