/* 
 * Persistence4J - Simple library for data persistence using java
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.common;

import java.sql.SQLException;


/**
 * @author Avdhesh Yadav - avdhesh.yadav@gmail.com    
 */
public class DAOException extends SQLException
{
	private static final long serialVersionUID = 1L;
	
	public DAOException()
	{
		super();
	}

	public DAOException(String message)
	{
		super(message);
	}
	
	public DAOException(String message,Throwable th)
	{
		super(message);
		th.printStackTrace();
	}

	public DAOException(String message, String SQLState, int vendorCode)
	{
		super(message, SQLState, vendorCode);
	}
}
