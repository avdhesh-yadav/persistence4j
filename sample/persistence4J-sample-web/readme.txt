Following Are the Steps to Build and deploy sample Application


Apache Derby
------------
Step 1
-------
The default persistence4J-sample-web comes with Embedded Apache Derby Database so no need to do any import of database.
Data will be automatically imported at the time of start up.

Jump to the Step 2.


PostgresSQL/MySQL
-----------------
Step 1
------
Import persistence4J-sample-web/sq/mysql_sample_data.sql into the MySql in order to create Database tables.In case of PostgresSQL
import postgres_sample_data.sql into the PostgresSQL.
This important will create all the table and insert sample into the library database.
 

Step 2
------
Next open /persistence4J-sample-web/war/WEB-INF/persistence.properties and set username and password 
for the MySql.By Default it is set to mysql as username and mysql as password.Same steps for the PostgresSQL.
You should change it as per your installation.

Step 3
------
In this step we build the Sample Application.Make sure that Maven is installed on your machine.

$ mvn clean install

This command will first download all the required dependencies for the sample application,Once all the 
Dependencies downloaded it will compile and build the war file.

Copy persistence4J-sample-web. war from persistence4J-sample-web/target folder and paste it in webapps folder
of your tomcat.

Step 4
------
Next step is to start tomcat and point your browser to http://localhost:8080/persistence4J-sample-web



If you Find any difficulty in building or deploying sample Application.Post your questions on the Discussion forum
http://groups.google.com/group/persistence4j

Thanks
Avdhesh Yadav