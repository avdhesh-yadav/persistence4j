--
-- PostgreSQL database dump
--

-- Started on 2010-08-25 10:20:26 IST

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 6 (class 2615 OID 16424)
-- Name: library; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA library;


ALTER SCHEMA library OWNER TO postgres;

SET search_path = library, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1498 (class 1259 OID 16425)
-- Dependencies: 6
-- Name: author; Type: TABLE; Schema: library; Owner: postgres; Tablespace: 
--

CREATE TABLE author (
    authorid integer NOT NULL,
    author_name character varying(45) NOT NULL
);


ALTER TABLE library.author OWNER TO postgres;

--
-- TOC entry 1499 (class 1259 OID 16428)
-- Dependencies: 6
-- Name: book; Type: TABLE; Schema: library; Owner: postgres; Tablespace: 
--

CREATE TABLE book (
    isbn character varying(45) NOT NULL,
    title character varying(128) NOT NULL,
    authorid integer NOT NULL
);


ALTER TABLE library.book OWNER TO postgres;

--
-- TOC entry 1500 (class 1259 OID 16431)
-- Dependencies: 6
-- Name: book_inventory; Type: TABLE; Schema: library; Owner: postgres; Tablespace: 
--

CREATE TABLE book_inventory (
    isbn character varying(45) NOT NULL,
    no_available integer NOT NULL,
    totalbooks integer NOT NULL
);


ALTER TABLE library.book_inventory OWNER TO postgres;

--
-- TOC entry 1501 (class 1259 OID 16434)
-- Dependencies: 6
-- Name: book_issue; Type: TABLE; Schema: library; Owner: postgres; Tablespace: 
--

CREATE TABLE book_issue (
    isbn character varying(45) NOT NULL,
    rollno integer NOT NULL,
    date_of_issue timestamp without time zone NOT NULL,
    date_of_return timestamp without time zone
);


ALTER TABLE library.book_issue OWNER TO postgres;

--
-- TOC entry 1502 (class 1259 OID 16437)
-- Dependencies: 6
-- Name: student; Type: TABLE; Schema: library; Owner: postgres; Tablespace: 
--

CREATE TABLE student (
    rollno integer NOT NULL,
    name character varying(45) NOT NULL
);


ALTER TABLE library.student OWNER TO postgres;

--
-- TOC entry 1790 (class 0 OID 16425)
-- Dependencies: 1498
-- Data for Name: author; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY author (authorid, author_name) FROM stdin;
1	william grosso
2	joshua bloch
3	Richard Monson
4	Morris Mano
5	Eric Evans
\.


--
-- TOC entry 1791 (class 0 OID 16428)
-- Dependencies: 1499
-- Data for Name: book; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY book (isbn, title, authorid) FROM stdin;
81-203-0855-7	Computer System Architecture	4
81-7366-270-3	Enterprise JavaBeans	3
81-7366-381-5	Java RMI	1
978-0-321-35668-0	Effective Java	2
978-81-317-2446-0	Domain Driven Design	5
\.


--
-- TOC entry 1792 (class 0 OID 16431)
-- Dependencies: 1500
-- Data for Name: book_inventory; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY book_inventory (isbn, no_available, totalbooks) FROM stdin;
81-203-0855-7	3	3
81-7366-270-3	2	2
81-7366-381-5	1	1
978-0-321-35668-0	4	4
978-81-317-2446-0	2	2
\.


--
-- TOC entry 1793 (class 0 OID 16434)
-- Dependencies: 1501
-- Data for Name: book_issue; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY book_issue (isbn, rollno, date_of_issue, date_of_return) FROM stdin;
\.


--
-- TOC entry 1794 (class 0 OID 16437)
-- Dependencies: 1502
-- Data for Name: student; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY student (rollno, name) FROM stdin;
401	Ajay
402	John
403	Adrian Antal
404	Avdhesh
\.


--
-- TOC entry 1781 (class 2606 OID 16442)
-- Dependencies: 1498 1498
-- Name: author_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY author
    ADD CONSTRAINT author_pkey PRIMARY KEY (authorid);


--
-- TOC entry 1785 (class 2606 OID 16446)
-- Dependencies: 1500 1500
-- Name: book_inventory_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY book_inventory
    ADD CONSTRAINT book_inventory_pkey PRIMARY KEY (isbn);


--
-- TOC entry 1787 (class 2606 OID 16448)
-- Dependencies: 1501 1501 1501
-- Name: book_issue_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY book_issue
    ADD CONSTRAINT book_issue_pkey PRIMARY KEY (isbn, rollno);


--
-- TOC entry 1783 (class 2606 OID 16444)
-- Dependencies: 1499 1499
-- Name: book_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY book
    ADD CONSTRAINT book_pkey PRIMARY KEY (isbn);


--
-- TOC entry 1789 (class 2606 OID 16450)
-- Dependencies: 1502 1502
-- Name: student_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student
    ADD CONSTRAINT student_pkey PRIMARY KEY (rollno);


--
-- TOC entry 1798 (class 0 OID 0)
-- Dependencies: 6
-- Name: library; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA library FROM PUBLIC;
REVOKE ALL ON SCHEMA library FROM postgres;
GRANT ALL ON SCHEMA library TO postgres;
GRANT ALL ON SCHEMA library TO PUBLIC;


--
-- TOC entry 1799 (class 0 OID 0)
-- Dependencies: 1498
-- Name: author; Type: ACL; Schema: library; Owner: postgres
--

REVOKE ALL ON TABLE author FROM PUBLIC;
REVOKE ALL ON TABLE author FROM postgres;
GRANT ALL ON TABLE author TO postgres;
GRANT ALL ON TABLE author TO PUBLIC;


--
-- TOC entry 1800 (class 0 OID 0)
-- Dependencies: 1499
-- Name: book; Type: ACL; Schema: library; Owner: postgres
--

REVOKE ALL ON TABLE book FROM PUBLIC;
REVOKE ALL ON TABLE book FROM postgres;
GRANT ALL ON TABLE book TO postgres;
GRANT ALL ON TABLE book TO PUBLIC;


--
-- TOC entry 1801 (class 0 OID 0)
-- Dependencies: 1500
-- Name: book_inventory; Type: ACL; Schema: library; Owner: postgres
--

REVOKE ALL ON TABLE book_inventory FROM PUBLIC;
REVOKE ALL ON TABLE book_inventory FROM postgres;
GRANT ALL ON TABLE book_inventory TO postgres;
GRANT ALL ON TABLE book_inventory TO PUBLIC;


--
-- TOC entry 1802 (class 0 OID 0)
-- Dependencies: 1501
-- Name: book_issue; Type: ACL; Schema: library; Owner: postgres
--

REVOKE ALL ON TABLE book_issue FROM PUBLIC;
REVOKE ALL ON TABLE book_issue FROM postgres;
GRANT ALL ON TABLE book_issue TO postgres;
GRANT ALL ON TABLE book_issue TO PUBLIC;


--
-- TOC entry 1803 (class 0 OID 0)
-- Dependencies: 1502
-- Name: student; Type: ACL; Schema: library; Owner: postgres
--

REVOKE ALL ON TABLE student FROM PUBLIC;
REVOKE ALL ON TABLE student FROM postgres;
GRANT ALL ON TABLE student TO postgres;
GRANT ALL ON TABLE student TO PUBLIC;


-- Completed on 2010-08-25 10:20:26 IST

--
-- PostgreSQL database dump complete
--

