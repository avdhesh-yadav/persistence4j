CREATE DATABASE  IF NOT EXISTS `library`;
USE `library`;

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `isbn` varchar(45) NOT NULL,
  `title` varchar(128) NOT NULL,
  `authorid` int(11) NOT NULL,
  PRIMARY KEY (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES ('81-203-0855-7','Computer System Architecture',4),('81-7366-270-3','Enterprise JavaBeans',3),('81-7366-381-5','Java RMI',1),('978-0-321-35668-0','Effective Java',2),('978-81-317-2446-0','Domain Driven Design',5);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_issue`
--

DROP TABLE IF EXISTS `book_issue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_issue` (
  `isbn` varchar(45) NOT NULL,
  `rollno` int(11) NOT NULL,
  `date_of_issue` datetime NOT NULL,
  `date_of_return` datetime DEFAULT NULL,
  PRIMARY KEY (`isbn`,`rollno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_issue`
--

LOCK TABLES `book_issue` WRITE;
/*!40000 ALTER TABLE `book_issue` DISABLE KEYS */;
/*!40000 ALTER TABLE `book_issue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Definition of table `library`.`book_inventory`
--

DROP TABLE IF EXISTS `library`.`book_inventory`;
CREATE TABLE  `library`.`book_inventory` (
  `isbn` varchar(45) NOT NULL,
  `no_available` int(11) NOT NULL,
  `totalbooks` int(11) NOT NULL,
  PRIMARY KEY (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `library`.`book_inventory`
--

/*!40000 ALTER TABLE `book_inventory` DISABLE KEYS */;
LOCK TABLES `book_inventory` WRITE;
INSERT INTO `library`.`book_inventory` VALUES  ('81-203-0855-7',3,3),
 ('81-7366-270-3',2,2),
 ('81-7366-381-5',1,1),
 ('978-0-321-35668-0',4,4),
 ('978-81-317-2446-0',2,2);
UNLOCK TABLES;
/*!40000 ALTER TABLE `book_inventory` ENABLE KEYS */;


--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `rollno` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`rollno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (401,'Ajay'),(402,'John'),(403,'Adrian Antal'),(404,'Avdhesh');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author` (
  `authorid` int(11) NOT NULL,
  `author_name` varchar(45) NOT NULL,
  PRIMARY KEY (`authorid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (1,'william grosso'),(2,'joshua bloch'),(3,'Richard Monson'),(4,'Morris Mano'),(5,'Eric Evans');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;
