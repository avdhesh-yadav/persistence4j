/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.server.guice;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import com.avdy.p4j.common.DbmsType;
import com.avdy.p4j.jdbc.PersistenceConfig;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

/**
 * 
 * @author Avdhesh Yadav
 *
 */
public class GuiceServletConfig extends GuiceServletContextListener
{
	private ServletContext context;

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) 
	{
		context = null;
		super.contextDestroyed(servletContextEvent);
	}


	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) 
	{
		context = servletContextEvent.getServletContext();
		
		try
		{
			Properties properties = loadPropertiesFile();
			System.out.println("properties:"+properties);
			String dbmsName = properties.getProperty("use.jdbc.dbms");
			
			// Sample data loaded only in case of Apache Derby and HSQLDB.In case of MySQL or PostgresSQL manually the import the sample database.
			// Mysql and PostgresSQL dump provided with the Sample Application
			if(dbmsName.equalsIgnoreCase(DbmsType.DERBY) || dbmsName.equalsIgnoreCase(DbmsType.HSQLDB))
			{
				PersistenceConfig config = new PersistenceConfig(properties);
				String databaseName = properties.getProperty("use.jdbc.database");
				SampleDataLoader dataLoader =  new SampleDataLoader(databaseName, dbmsName, config);
				dataLoader.createLibraryTables();
				dataLoader.insertRows();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
		
		super.contextInitialized(servletContextEvent);
	}


	@Override
	protected Injector getInjector() 
	{
		return Guice.createInjector(new GuiceServerModule(context.getRealPath("/")),new DispatchServletModule());
	}
	
	 /**
     * 
     * @param context Properties
     * 
     * @return ServletContext
     * 
     * @throws Exception 
     */
    private Properties loadPropertiesFile() throws Exception
    {
    	String propertiesFile = context.getRealPath("/") + "WEB-INF/" + "persistence.properties";
    	InputStream inputStream = new FileInputStream(propertiesFile);
    	Properties properties = new Properties();
		properties.load(inputStream);
		inputStream.close();
		return properties;
    }
}