/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.server.handler;

import java.util.ArrayList;
import java.util.List;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.ActionException;

import com.avdy.p4j.common.DAOException;
import com.avdy.p4j.sample.server.dao.BookRepository;
import com.avdy.p4j.sample.server.dao.LibraryDataRepository;
import com.avdy.p4j.sample.shared.data.Book;
import com.avdy.p4j.sample.shared.rpc.BooksResult;
import com.avdy.p4j.sample.shared.rpc.FetchBooksAction;
import com.google.inject.Inject;

public class FetchBooksHandler implements ActionHandler<FetchBooksAction, BooksResult>
{
	private final LibraryDataRepository dataRepository;

	@Inject
	public FetchBooksHandler(final LibraryDataRepository dataRepository)
	{
		this.dataRepository = dataRepository;
	}

	@Override
	public BooksResult execute(final FetchBooksAction action,final ExecutionContext context) throws ActionException 
	{
		BookRepository bookRepository = dataRepository.getBookRepository();
		boolean isFethAllBooks = action.isFetchAllBooks();
		List<Book> books = new ArrayList<Book>();	
		try 
		{
			if(isFethAllBooks)
			{
				books = (List<Book>)bookRepository.findAll(Book.class, bookRepository.initialPosition, bookRepository.ALL_ROWS);
			}
			else
			{
				books = bookRepository.getAllIssuedBooks();
			}
		} catch (DAOException e) 
		{
			e.printStackTrace();
		}
		BooksResult result = new BooksResult(books);
		return result;
	}

	
	@Override
	public void rollback(final FetchBooksAction action,final BooksResult result, final ExecutionContext context)throws ActionException 
	{
		// Nothing to do here
	}

	@Override
	public Class<FetchBooksAction> getActionType() 
	{
		return FetchBooksAction.class;
	}
}
