/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.server.dao;

import com.avdy.p4j.common.DbmsType;
import com.avdy.p4j.jdbc.service.DataProvider;
import com.avdy.p4j.jdbc.service.GenericDAO;
import com.avdy.p4j.jdbc.service.GenericDaoImpl;
import com.avdy.p4j.jdbc.transfer.TransferUtil;
import com.avdy.p4j.sample.server.dao.derby.DerbySampleDataRepository;
import com.avdy.p4j.sample.server.dao.hsqldb.HsqlSampleDataRepositoryImpl;
import com.avdy.p4j.sample.server.dao.mysql.MySqlSampleDataRepositoryImpl;
import com.avdy.p4j.sample.server.dao.pgsql.PgSqlSampleDataRepositoryImpl;
import com.avdy.p4j.sample.shared.data.Author;
import com.avdy.p4j.sample.shared.data.Book;
import com.avdy.p4j.sample.shared.data.BookInventory;
import com.avdy.p4j.sample.shared.data.BookIssue;
import com.avdy.p4j.sample.shared.data.Student;

/**
 * 
 * @author Avdhesh Yadav
 *
 */
public abstract class LibraryDataRepository 
{
	//
	public abstract StudentRepository getStudentRepository();	
	//
	public abstract BookRepository getBookRepository();
	//
	protected DataProvider dataProvider;
		
	/**
	 * 
	 * @param dataFetcher DataFetcher
	 * 
	 * @throws Exception
	 */
	public LibraryDataRepository(DataProvider dataProvider) throws Exception
	{
		this.dataProvider = dataProvider;
		TransferUtil.registerClass(Author.class);
		TransferUtil.registerClass(Book.class);
		TransferUtil.registerClass(Student.class);
		TransferUtil.registerClass(BookIssue.class);
		TransferUtil.registerClass(BookInventory.class);
	}
	
	
	/**
	 * 
	 * @param dataFetcher DataFetcher
	 * 
	 * @return SampleDataRepository
	 * 
	 * @throws Exception
	 */
	public static LibraryDataRepository getLibraryDataRepository(DataProvider dataProvider) throws Exception
	{
		String dbmsName = dataProvider.getDbms();
		if(dbmsName.equalsIgnoreCase(DbmsType.MYSQL))
			return new MySqlSampleDataRepositoryImpl(dataProvider);
		else if(dbmsName.equalsIgnoreCase(DbmsType.PGSQL))
			return new PgSqlSampleDataRepositoryImpl(dataProvider);
		else if(dbmsName.equalsIgnoreCase(DbmsType.DERBY))
			return new DerbySampleDataRepository(dataProvider);
		else if(dbmsName.equalsIgnoreCase(DbmsType.HSQLDB))
			return new HsqlSampleDataRepositoryImpl(dataProvider);
		else 
			throw new RuntimeException("Implementation for this repository is not found");
	}
	
	
	/**
	 * 
	 * @return GenericDaoImpl
	 */
	public <E> GenericDAO<E> getGenericDaoImpl()
	{
		return new GenericDaoImpl<E>(dataProvider.getDataFetcher());
	}
	
	/**
	 * 
	 * @return DataProvider
	 */
	public DataProvider getDataProvider()
	{
		return dataProvider;
	}
}
