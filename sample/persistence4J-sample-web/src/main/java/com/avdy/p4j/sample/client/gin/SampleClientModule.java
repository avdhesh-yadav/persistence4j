/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.client.gin;

import com.avdy.p4j.sample.client.place.BookListPresenterPlace;
import com.avdy.p4j.sample.client.place.SamplePlaceManager;
import com.avdy.p4j.sample.client.presenter.AppPresenter;
import com.avdy.p4j.sample.client.presenter.BookListPresenter;
import com.avdy.p4j.sample.client.presenter.MainPresenter;
import com.avdy.p4j.sample.client.view.AppView;
import com.avdy.p4j.sample.client.view.BookListView;
import com.avdy.p4j.sample.client.view.MainView;
import com.google.inject.Singleton;

import net.customware.gwt.presenter.client.DefaultEventBus;
import net.customware.gwt.presenter.client.EventBus;
import net.customware.gwt.presenter.client.gin.AbstractPresenterModule;
import net.customware.gwt.presenter.client.place.PlaceManager;

public class SampleClientModule  extends AbstractPresenterModule 
{
	@Override
    protected void configure() 
    {        
        bind(EventBus.class).to(DefaultEventBus.class).in(Singleton.class);
        bind(PlaceManager.class).to(SamplePlaceManager.class);
        bindPresenter(AppPresenter.class, AppPresenter.Display.class, AppView.class);
        bindPresenter(MainPresenter.class, MainPresenter.Display.class, MainView.class);
        bindPresenter(BookListPresenter.class, BookListPresenter.Display.class, BookListView.class);
        bind(BookListPresenterPlace.class).in(Singleton.class);
    }
}
