/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.shared.data;

import java.io.Serializable;

import com.avdy.p4j.jdbc.model.Column;
import com.avdy.p4j.jdbc.model.Entity;

@Entity(schema="library",table="author")
public class Author implements Serializable
{
	@Column(isPrimaryKey= true)
	private int authorid;
	@Column
	private String author_name;

	/**
	 * 
	 */
	public Author()
	{

	}

	/**
	 * 
	 * @param authorId int
	 * @param author_name String
	 */
	public Author(int authorId, String author_name)
	{
		this.authorid = authorId;
		this.author_name = author_name;
	}


	/**
	 * 
	 * @return int
	 */
	public int getAuthorId()
	{
		return authorid;
	}

	
	/**
	 * 
	 * @return String
	 */
	public String getAuthorName()
	{
		return author_name;
	}
}