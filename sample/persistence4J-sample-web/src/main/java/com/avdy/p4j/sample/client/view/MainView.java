/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.client.view;

import net.customware.gwt.presenter.client.EventBus;

import com.avdy.p4j.sample.client.SampleCSS;
import com.avdy.p4j.sample.client.presenter.MainPresenter;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

public class MainView extends Composite implements MainPresenter.Display
{
	private DockPanel dockPanel;
	private VerticalPanel north;
	private DecoratorPanel center;
	private Tree tree = new Tree();
	
	private DecoratorPanel west;
	private VerticalPanel treePanel = new VerticalPanel();
	private SimplePanel panel = new SimplePanel();
	private EventBus bus;
	
	@Inject
	public MainView(EventBus bus)
	{
		this.bus = bus;

		dockPanel = new DockPanel();
		dockPanel.setSpacing(3);
		dockPanel.setWidth("100%");

		createNorth();
		createWest();
		createCenter();
		
		dockPanel.add(north, DockPanel.NORTH);
        dockPanel.add(west, DockPanel.WEST);
        dockPanel.setCellWidth(west, "100px");
        dockPanel.add(center, DockPanel.CENTER);
        dockPanel.setCellHorizontalAlignment(north, DockPanel.ALIGN_RIGHT);
        dockPanel.setCellHorizontalAlignment(center, DockPanel.ALIGN_LEFT);

		initWidget(dockPanel);
	}

	
	/**
	 * 
	 */
	private void createNorth()
	{
		north = new VerticalPanel();
		north.setWidth("100%");
	}

	
	/**
	 * 
	 */
	private void createCenter()
	{
		center = new DecoratorPanel();
		center.setWidth("100%");
	}

	
	/**
	 * 
	 */
	private void createWest() 
	{
		west = new DecoratorPanel();
		west.add(tree);
		west.addStyleName(SampleCSS.C_tree_container);
		
		createMainMenu();
		
		tree.setAnimationEnabled(true);
		treePanel.setSpacing(5);

		treePanel.add(tree);
		panel.add(treePanel);
		west.add(panel);
	}
	
	/**
	 * 
	 */
	private void createMainMenu()
	{
	    tree.addItem(ALL_BOOKS);
	    tree.addItem(ISSUED_BOOKS);
	    tree.addItem(ISSUE_BOOK);
	    tree.addItem(RETURN_BOOK);
	}

	/**
	 * 
	 */
	public Widget asWidget()
	{
		return this;
	}

	/**
	 * 
	 */
	public HasSelectionHandlers<TreeItem> getTree()
	{
		return tree;
	}
	
	/**
	 * 
	 */
	public void addWidget(Widget widget)
	{
		center.clear();
		center.add(widget);
	}
	
	/**
	 * 
	 */
	public void removeWidget(Widget widget) 
	{
		center.remove(widget);
	}

	/**
	 * 
	 */
	public void showWidget(Widget widget)
	{
		center.clear();
		center.add(widget);
	}
}
