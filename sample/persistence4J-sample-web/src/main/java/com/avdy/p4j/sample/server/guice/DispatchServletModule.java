/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.server.guice;

import com.google.inject.servlet.ServletModule;

import net.customware.gwt.dispatch.server.guice.GuiceStandardDispatchServlet;

public class DispatchServletModule extends ServletModule
{
	private static final String SERVLET_DISPATCH = "dispatch";
	
	private static final String WEBAPP = "sample";
	
     @Override
     public void configureServlets()
     {
            serve("/" + WEBAPP + "/" + SERVLET_DISPATCH).with(GuiceStandardDispatchServlet.class );
     }
}
