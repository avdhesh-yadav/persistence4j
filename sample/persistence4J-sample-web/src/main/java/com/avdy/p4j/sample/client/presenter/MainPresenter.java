/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.client.presenter;

import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.inject.Inject;

import net.customware.gwt.dispatch.client.DispatchAsync;
import net.customware.gwt.presenter.client.EventBus;
import net.customware.gwt.presenter.client.widget.WidgetContainerDisplay;
import net.customware.gwt.presenter.client.widget.WidgetContainerPresenter;

public class MainPresenter extends WidgetContainerPresenter<MainPresenter.Display>
{
	public interface Display extends WidgetContainerDisplay
	{
		public String ALL_BOOKS = "All Books";
		public String ISSUED_BOOKS = "Issued Books";
		public String ISSUE_BOOK = "Issue Book";
		public String RETURN_BOOK = "Return Book";
		
		public HasSelectionHandlers<TreeItem> getTree();
	}

	private DispatchAsync dispatcher;
	private BookListPresenter bookListPresenter;

	@Inject
	public MainPresenter(MainPresenter.Display display, DispatchAsync dispatcher, final EventBus bus, BookListPresenter bookListPresenter) 
	{
		super(display, bus,bookListPresenter);
		this.dispatcher = dispatcher;  
		this.bookListPresenter = bookListPresenter;
	}


	@Override
	protected void onBind()
	{  
		super.onBind();
		registerHandler(display.getTree().addSelectionHandler(new SelectionHandler<TreeItem>() {

			public void onSelection(SelectionEvent<TreeItem> event) 
			{
				TreeItem tItem = event.getSelectedItem();
				String item = (String)tItem.getText();
			}
		}));
	}
}