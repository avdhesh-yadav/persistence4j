/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.shared.rpc;

import java.util.List;

import net.customware.gwt.dispatch.shared.Action;

public class ReturnBookAction implements Action<GenericResult>
{
	private List<String> isbnList;
	private int rollno;

	public ReturnBookAction()
	{

	}

	public ReturnBookAction(List<String>isbnList, int rollno )
	{
		this.isbnList = isbnList;
		this.rollno  = rollno;
	}

	public List<String> getIsbns()
	{
		return isbnList;
	}


	public int getRollNo()
	{
		return rollno;
	}
}
