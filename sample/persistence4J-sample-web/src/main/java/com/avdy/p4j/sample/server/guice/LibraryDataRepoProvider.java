/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.server.guice;

import com.avdy.p4j.jdbc.service.DataProvider;
import com.avdy.p4j.jdbc.service.DataProviderFactory;
import com.avdy.p4j.sample.server.dao.LibraryDataRepository;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
public class LibraryDataRepoProvider  implements Provider<LibraryDataRepository>
{
	private DataProviderFactory dataProviderFactory;
	private String databaseName;
	private String dbmsName;
	private boolean isTransactional;
	
	@Inject
	public LibraryDataRepoProvider(DataProviderFactory dataProviderFactory,@Named("use.jdbc.database") String databaseName, @Named("use.jdbc.dbms") String dbmsName,@Named("use.jdbc.transactional") boolean isTransactional)
	{
		this.dataProviderFactory = dataProviderFactory;
		this.databaseName = databaseName;
		this.dbmsName = dbmsName;
		this.isTransactional = isTransactional;
	}
	
	
	/**
	 * 
	 */
	public LibraryDataRepository get()
	{
		try 
		{
			DataProvider dataProvider =  dataProviderFactory.getDataProvider(databaseName, dbmsName, isTransactional);
			return LibraryDataRepository.getLibraryDataRepository(dataProvider);
		}
		catch (Throwable e)
		{
			e.printStackTrace();
            throw new RuntimeException(e.getMessage());
		}
	}
}
