/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.shared.data;

import java.io.Serializable;
import java.util.Date;

import com.avdy.p4j.jdbc.model.Column;
import com.avdy.p4j.jdbc.model.Entity;

@Entity(schema="library",table = "book_issue")
public class BookIssue implements Serializable
{
	@Column(isPrimaryKey=true)
	private int isbn;
	@Column(isPrimaryKey = true)
	private int rollno;
	@Column
	private Date date_of_issue;
	@Column
	private Date date_of_return;

	public BookIssue()
	{

	}


	public BookIssue(int isbn, int rollno, Date date_of_issue)
	{
		this.isbn = isbn;
		this.rollno = rollno;
		this.date_of_issue = date_of_issue;
	}


	public void setDateOfReturn(Date date_of_return)
	{
		this.date_of_return = date_of_return;
	}


	public int getISBN()
	{
		return isbn;
	}


	public int getRollNo()
	{
		return rollno;
	}


	public Date getDateOfIssue()
	{
		return date_of_issue;
	}

	public Date getDateOfReturn()
	{
		return date_of_return;
	}

}
