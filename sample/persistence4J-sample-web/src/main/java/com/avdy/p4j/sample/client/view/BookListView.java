/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.avdy.p4j.sample.client.view;


import org.gwt.mosaic.override.client.FlexTable;
import org.gwt.mosaic.override.client.FlexTable.FlexCellFormatter;
import org.gwt.mosaic.ui.client.table.AbstractScrollTable.ColumnResizePolicy;
import org.gwt.mosaic.ui.client.table.AbstractScrollTable.ResizePolicy;
import org.gwt.mosaic.ui.client.table.FixedWidthFlexTable;
import org.gwt.mosaic.ui.client.table.FixedWidthGrid;
import org.gwt.mosaic.ui.client.table.ScrollTable;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Widget;
import com.avdy.p4j.sample.client.presenter.BookListPresenter;

public class BookListView extends Composite implements BookListPresenter.Display
{
	private ScrollTable scrollTable;
	private FlexTable layout = new FlexTable();

	public BookListView()
	{
		layout.setWidth("600px");
		layout.setCellPadding(0);
		layout.setCellSpacing(5);
		FlexCellFormatter formatter = layout.getFlexCellFormatter();

		FixedWidthGrid dataTable = new FixedWidthGrid(1,3);

		FixedWidthFlexTable headerTable = new FixedWidthFlexTable();
		headerTable.setHTML(0, 0, "ISBN");
		headerTable.setHTML(0, 1, "Title");
		headerTable.setHTML(0, 2, "Author");
		scrollTable = new ScrollTable(dataTable, headerTable);
		scrollTable.setResizePolicy(ResizePolicy.FIXED_WIDTH);
		scrollTable.setColumnResizePolicy(ColumnResizePolicy.DISABLED);
		scrollTable.setHeight("400px");
		scrollTable.setCellPadding(10);
		layout.setWidget(0, 1, scrollTable);
		formatter.setWidth(0, 1, "100%");
	    formatter.setVerticalAlignment(0, 1, HasVerticalAlignment.ALIGN_TOP);
		initWidget(layout);
	}


	/**
	 * 
	 */
	public FixedWidthGrid getDataTable()
	{
		return scrollTable.getDataTable();
	}


	public Widget asWidget()
	{
		return this;
	}
}
