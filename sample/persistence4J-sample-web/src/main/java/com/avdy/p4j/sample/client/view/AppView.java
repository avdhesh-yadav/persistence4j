/* 
 * Copyright (c) 2010, Avdhesh yadav.
 * http://www.avdheshyadav.com
 * Contact: avdhesh.yadav@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

package com.avdy.p4j.sample.client.view;

import com.avdy.p4j.sample.client.SampleCSS;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.avdy.p4j.sample.client.presenter.AppPresenter;

public class AppView extends Composite implements AppPresenter.Display
{
	private FlowPanel appContainer = new FlowPanel();
	private SimplePanel topWrapper = new SimplePanel();
	private SimplePanel topContainer = new SimplePanel();
	private SimplePanel centralContainer = new SimplePanel();
	private FlowPanel bottomContainer = new FlowPanel();
    private HTML footLabel = new HTML();
	/**
	 * 
	 */
	public AppView()
	{
		appContainer.addStyleName(SampleCSS.C_app_container);
		topContainer.addStyleName(SampleCSS.C_top_container);
		bottomContainer.addStyleName(SampleCSS.C_bottom_container);
		createTopContainer();
		
		footLabel.setHTML("Demo Application Developed using <a target=_blank href=http://code.google.com/p/persistence4j>Persistence4J</a> library for Data Persistence and GWT for Presentation");
		bottomContainer.add(footLabel);
		
		appContainer.add(topContainer);
		appContainer.add(centralContainer);
		appContainer.add(bottomContainer);
		initWidget(appContainer);
	}
	
	
	/**
	 * 
	 */
	private void createTopContainer()
	{
		Label label = new Label("Sample Library Management Application");
		label.getElement().getStyle().setFontSize(20, Unit.PX);
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);
		horizontalPanel.add(label);
		topWrapper.add(horizontalPanel);
		topWrapper.addStyleName(SampleCSS.C_top_container + "-wrap");
		topContainer.add(topWrapper);
	}
	
	
	/**
	 * 
	 */
    public Widget asWidget()
    {
        return this;
    }
    
    
    /**
     * 
     */
    public void addWidget(Widget widget)
    {
    	centralContainer.clear();
    	centralContainer.add(widget);
    }

    /**
     * 
     */
    public void removeWidget(Widget widget) 
    {
    	centralContainer.remove(widget);
    }

    /**
     * 
     */
    public void showWidget(Widget widget)
    {
    	centralContainer.clear();
    	centralContainer.add(widget);
    }
}
